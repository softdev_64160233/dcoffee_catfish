/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.service.MaterialService;
import com.Dcoffee.catfish.service.StockImportService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import static java.awt.image.ImageObserver.HEIGHT;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author User
 */
public class AddStockImportDialog extends javax.swing.JDialog {

    private MaterialService matService;
    private List<Material> selectedMaterials = new ArrayList<>();
    private List<Material> matList;
    StockImportService hisInvoiceService = new StockImportService();
    private StockImport hisInvoice;
    private ArrayList<StockImportDetail> hisInvoiceDetails = new ArrayList<StockImportDetail>();

    //private final StockImportService stockImportService;
    private StockImport editedStockImport;

    /**
     * Creates new form AddStockImportDialog
     */
    public AddStockImportDialog(java.awt.Frame parent, StockImport editedStockImport) {
        super(parent, true);
        this.setUndecorated(true);
        initComponents();
        tblStock.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblStockImport.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        this.editedStockImport = editedStockImport;
        btnDelete.setIcon(new ImageIcon("img/deleteW.png"));
        btnSave .setIcon(new ImageIcon("img/addW.png"));
        btnClear.setIcon(new ImageIcon("img/clearW.png"));
        hisInvoice = new StockImport();
        hisInvoice.setUser(UserService.getCurrentUser());
        initStockImportTable();
        tblStockImport.setRowHeight(30);
        tblStockImport.setModel(new AbstractTableModel() {
            String[] hearderHis = {"Name", "Company", "Price", "QTY", "Total"};

            @Override
            public int getRowCount() {
                return hisInvoice.getStockImportDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public String getColumnName(int column) {
                return hearderHis[column];
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
                StockImportDetail hisInvoiceDetail = hisInvoiceDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisInvoiceDetail.getMaterialName();
                    case 1:
                        return hisInvoiceDetail.getStockImportDetailCompany();
                    case 2:
                        return hisInvoiceDetail.getMaterialPrice();
                    case 3:
                        return hisInvoiceDetail.getMaterialQty();
                    case 4:
                        return hisInvoiceDetail.getMaterialTotalPrice();
                    default:
                        return "";
                }
            }

            //tblStockImport.setModel(tblImportModel);
            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
                StockImportDetail hisInvoiceDetail = hisInvoiceDetails.get(rowIndex);
                if (columnIndex == 3) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    hisInvoiceDetail.setMaterialQty(qty);
                    hisInvoiceDetail.setMaterialTotalPrice(hisInvoiceDetail.getMaterialPrice() * qty); // อัปเดต materialTotalPrice
                    hisInvoice.calculateTotal();
                    refresHis();
                }
                if (columnIndex == 1) {
                    String company = (String) aValue;
                    hisInvoiceDetail.setStockImportDetailCompany(company);
                    refresHis();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 1:
                        return true;
                    case 3:
                        return true;
                    default:
                        return false;
                }
            }

            private void refresHis() {
                hisInvoice.calculateTotal();
                tblStockImport.repaint();
                tblStockImport.revalidate();
            }

        });

        tblStockImport.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(new JTextField()) {
            @Override
            public boolean stopCellEditing() {
                int rowIndex = tblStockImport.getEditingRow();
                int columnIndex = tblStockImport.getEditingColumn();

                if (columnIndex == 3) {
                    int newQuantity = Integer.parseInt((String) getCellEditorValue());
                    StockImportDetail detail = hisInvoice.getStockImportDetail().get(rowIndex);
                    detail.setMaterialQty(newQuantity);
                    hisInvoice.calculateTotal();
                    refresHis();
                }

                return super.stopCellEditing();
            }

            private void refresHis() {
                tblStockImport.revalidate();
                tblStockImport.repaint();
            }
        });

    }

    private void initStockImportTable() {
        matService = new MaterialService();
        matList = matService.getMaterials();
        tblStock.setRowHeight(30);
        tblStock.setModel(new AbstractTableModel() {
            String[] hearderHis = {"Name", "Unit"};

            @Override
            public String getColumnName(int column) {
                return hearderHis[column];
            }

            @Override
            public int getRowCount() {
                return matList.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material mat = matList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return mat.getMaterialName();
                    case 1:
                        return mat.getMaterialUnit();
                    default:
                        return "Unknown";
                }
            }

        });

        tblStock.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblStock.rowAtPoint(e.getPoint());
                int col = tblStock.columnAtPoint(e.getPoint());

                if (row >= 0 && col >= 0) {
                    Material matModel = matList.get(row);

                    boolean existsInHistory = false;
                    for (StockImportDetail detail : hisInvoice.getStockImportDetail()) {
                        if (detail.getMaterialId() == matModel.getMaterialID()) {
                            existsInHistory = true;
                            break;
                        }
                    }

                    if (!existsInHistory) {
                        // เช็คว่าวัสดุอยู่ใน selectedMaterials หรือไม่
                        boolean selectedMaterialExists = false;
                        for (Material selectedMaterial : selectedMaterials) {
                            if (selectedMaterial.getMaterialID() == matModel.getMaterialID()) {
                                selectedMaterialExists = true;
                                break;
                            }
                        }

                        if (selectedMaterialExists) {
                            // หาวัสดุที่เลือกแล้วและเพิ่มจำนวน
                            for (StockImportDetail detail : hisInvoice.getStockImportDetail()) {
                                if (detail.getMaterialId() == matModel.getMaterialID()) {
                                    detail.setMaterialQty(detail.getMaterialQty() + 1);
                                    hisInvoice.calculateTotal();
                                    refresHis();
                                    break;
                                }
                            }
                        } else {
                            // เพิ่มวัสดุเข้าในรายการ selectedMaterials
                            selectedMaterials.add(matModel);
                            hisInvoice.addStockImportDetail(matModel, 1);
                            tblStockImport.repaint();
                            tblStockImport.revalidate();
                        }
                    }
                }
            }

            private void refresHis() {
                tblStockImport.repaint();
                tblStockImport.revalidate();
            }

        });

    }

    public int calculateTotal(ArrayList<StockImportDetail> details) {
        int total = 0;
        for (StockImportDetail detail : details) {
            total += detail.getMaterialPrice() * detail.getMaterialQty();
        }
        System.out.print(total);
        return total;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblStockImport = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(65, 55, 54));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setBackground(new java.awt.Color(65, 55, 54));

        tblStockImport.setBackground(new java.awt.Color(211, 197, 186));
        tblStockImport.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblStockImport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Name", "Company", "Type", "Qty", "Total"
            }
        ));
        jScrollPane2.setViewportView(tblStockImport);

        tblStock.setBackground(new java.awt.Color(211, 197, 186));
        tblStock.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblStock);

        btnDelete.setBackground(new java.awt.Color(84, 30, 0));
        btnDelete.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(84, 50, 0));
        btnClear.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(84, 70, 63));
        btnSave.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(84, 70, 63));

        jLabel1.setBackground(new java.awt.Color(84, 70, 63));
        jLabel1.setFont(new java.awt.Font("Angsana New", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Import Material");

        jButton1.setBackground(new java.awt.Color(84, 70, 63));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("X");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(288, 288, 288)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 423, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15))
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSave)
                            .addComponent(btnDelete)
                            .addComponent(btnClear))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
        int input = JOptionPane.showConfirmDialog(this,
                "Do you want to clear ?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (input == 0) {
            hisInvoiceDetails.clear();

        }
        refreshStock();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedDel = tblStockImport.getSelectedRow();
        ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
        StockImportDetail hisInvoiceDetail = hisInvoiceDetails.get(selectedDel);
        if (selectedDel >= 0) {
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete " + hisInvoiceDetail.getMaterialName() + "?", "Caution!!", JOptionPane.YES_NO_OPTION,
                    JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                hisInvoice.delStockImportDetail(hisInvoiceDetail);
            }
            refreshStock();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
//        System.out.println(" " + hisInvoice);
        hisInvoiceService.add(hisInvoice);
        //scrMain.setViewportView(new HistoryInvoicePanel(scrMain));
        ImageIcon icon = new ImageIcon("./checked.png");
        Image image = icon.getImage(); // 
        Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
        icon = new ImageIcon(newimg); // 
        JOptionPane.showMessageDialog(tblStock, "Add material successfully!", "Material", HEIGHT, icon);
        this.dispose();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(AddStockImportDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(AddStockImportDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(AddStockImportDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(AddStockImportDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the dialog */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                AddStockImportDialog dialog = new AddStockImportDialog(new javax.swing.JFrame(), true);
//                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
//                    @Override
//                    public void windowClosing(java.awt.event.WindowEvent e) {
//                        System.exit(0);
//                    }
//                });
//                dialog.setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblStock;
    private javax.swing.JTable tblStockImport;
    // End of variables declaration//GEN-END:variables

    private void refreshStock() {
        tblStockImport.revalidate();
        tblStockImport.repaint();
    }
}

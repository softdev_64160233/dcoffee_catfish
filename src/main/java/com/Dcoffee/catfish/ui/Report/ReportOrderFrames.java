/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.Dcoffee.catfish.ui.Report;

import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportStock;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import com.Dcoffee.catfish.service.ReportService;
import com.Dcoffee.catfish.ui.DateLabelFormatter;
import java.awt.Color;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Miso
 */
public class ReportOrderFrames extends javax.swing.JFrame {
     private final ReportService reportService;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private List<ReportTotalCustomer> reportList;
    private List<ReportProduct> reportList1;
    private List<ReportStock> reportList2;
    private List<ReportSaleM> reportList3;
    private List<ReportSaleD> reportList4;
    private List<ReportSaleW> reportList5;
    private List<ReportSaleH> reportList6;
    private DefaultPieDataset Piedataset;

    /**
     * Creates new form ReportOrderFrames
     */
    public ReportOrderFrames() {
        this.setUndecorated(true);
        initComponents();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        scrChart.setViewportView(new ReportLinePanel());
        btnSearch.setIcon(new ImageIcon("img/search.png"));
        iconOrder.setIcon(new ImageIcon("img/Mcoffee.png"));
        reportService = new ReportService();
        reportList1 = reportService.getBestProduct();
        reportList = reportService.getTotalByCus();
        reportList2 = reportService.getOutStock();
        reportList3 = reportService.getReportSaleByMounth();
        reportList4 = reportService.getReportSaleByDay();
        reportList5 = reportService.getReportSaleByWeek();
        reportList6 = reportService.getReportSaleByHour();
        Search1();
        Search2();
//        System.out.println(""+reportList1);
        tblSaleW.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleM.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleH.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleD.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        initTableSaleM();
        initTableSaleD();
        initTableSaleW();
        initTableSaleH();
//        initPirChart();
//        loadPieBestSeller();

    }

    private void Search1() {
        model1 = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.mouth", "Mouth");
        p.put("text.year", "Year");
        JDatePanelImpl dataPanel = new JDatePanelImpl(model1, p);
        JDatePickerImpl dataPicker = new JDatePickerImpl(dataPanel, new DateLabelFormatter());
        pnlSearch1.add(dataPicker);
        model1.setSelected(true);
    }

    private void Search2() {
        model2 = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.mouth", "Mouth");
        p.put("text.year", "Year");
        JDatePanelImpl dataPanel = new JDatePanelImpl(model2, p);
        JDatePickerImpl dataPicker = new JDatePickerImpl(dataPanel, new DateLabelFormatter());
        pnlSearch2.add(dataPicker);
        model2.setSelected(true);
    }

    private void initTableSaleM() {
        tblSaleM.setModel(new AbstractTableModel() {
            String[] columnNames = {"Mounth", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList3.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleM report = reportList3.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getMounth();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleD() {
        tblSaleD.setModel(new AbstractTableModel() {
            String[] columnNames = {"Day", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList4.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleD report = reportList4.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getDay();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleW() {
        tblSaleW.setModel(new AbstractTableModel() {
            String[] columnNames = {"WeekStart", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList5.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleW report = reportList5.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getWeekStart();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleH() {
        tblSaleH.setModel(new AbstractTableModel() {
            String[] columnNames = {"HourStart", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList6.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleH report = reportList6.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getDay();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jPanel4 = new javax.swing.JPanel();
        jSlider1 = new javax.swing.JSlider();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        scrChart = new javax.swing.JScrollPane();
        plChart = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblSaleH = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblSaleD = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblSaleW = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblSaleM = new javax.swing.JTable();
        iconOrder = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnReportDate = new javax.swing.JButton();
        btnReportMounth = new javax.swing.JButton();
        btnReportWeek = new javax.swing.JButton();
        btnReportHour = new javax.swing.JButton();
        pnlSearch2 = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();
        pnlSearch1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        jToggleButton1.setText("jToggleButton1");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(84, 70, 63));

        jButton1.setBackground(new java.awt.Color(84, 70, 63));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("X");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Angsana New", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Order Report");

        jPanel1.setBackground(new java.awt.Color(65, 55, 54));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        plChart.setBackground(new java.awt.Color(156, 131, 112));
        plChart.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        plChart.setMaximumSize(new java.awt.Dimension(560, 367));
        plChart.setMinimumSize(new java.awt.Dimension(560, 367));

        javax.swing.GroupLayout plChartLayout = new javax.swing.GroupLayout(plChart);
        plChart.setLayout(plChartLayout);
        plChartLayout.setHorizontalGroup(
            plChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1389, Short.MAX_VALUE)
        );
        plChartLayout.setVerticalGroup(
            plChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1120, Short.MAX_VALUE)
        );

        scrChart.setViewportView(plChart);

        jPanel1.add(scrChart, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 90, 628, 340));

        jPanel6.setBackground(new java.awt.Color(65, 55, 54));
        jPanel6.setForeground(new java.awt.Color(65, 55, 54));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1267, 276, -1, -1));

        jPanel7.setBackground(new java.awt.Color(209, 196, 186));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jPanel5.setBackground(new java.awt.Color(209, 196, 186));

        tblSaleH.setBackground(new java.awt.Color(209, 196, 186));
        tblSaleH.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleH.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane7.setViewportView(tblSaleH);

        tblSaleD.setBackground(new java.awt.Color(209, 196, 186));
        tblSaleD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleD.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane5.setViewportView(tblSaleD);

        tblSaleW.setBackground(new java.awt.Color(209, 196, 186));
        tblSaleW.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleW.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane6.setViewportView(tblSaleW);

        tblSaleM.setBackground(new java.awt.Color(209, 196, 186));
        tblSaleM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleM.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane4.setViewportView(tblSaleM);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel5.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel5.setText("Report Date");

        btnReportDate.setBackground(new java.awt.Color(81, 68, 61));
        btnReportDate.setFont(new java.awt.Font("AngsanaUPC", 2, 36)); // NOI18N
        btnReportDate.setForeground(new java.awt.Color(255, 255, 255));
        btnReportDate.setText("Date");
        btnReportDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportDateActionPerformed(evt);
            }
        });

        btnReportMounth.setBackground(new java.awt.Color(81, 68, 61));
        btnReportMounth.setFont(new java.awt.Font("AngsanaUPC", 2, 36)); // NOI18N
        btnReportMounth.setForeground(new java.awt.Color(255, 255, 255));
        btnReportMounth.setText("Mounth");
        btnReportMounth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportMounthActionPerformed(evt);
            }
        });

        btnReportWeek.setBackground(new java.awt.Color(81, 68, 61));
        btnReportWeek.setFont(new java.awt.Font("AngsanaUPC", 2, 36)); // NOI18N
        btnReportWeek.setForeground(new java.awt.Color(255, 255, 255));
        btnReportWeek.setText("Week");
        btnReportWeek.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportWeekActionPerformed(evt);
            }
        });

        btnReportHour.setBackground(new java.awt.Color(81, 68, 61));
        btnReportHour.setFont(new java.awt.Font("AngsanaUPC", 2, 36)); // NOI18N
        btnReportHour.setForeground(new java.awt.Color(255, 255, 255));
        btnReportHour.setText("Time");
        btnReportHour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportHourActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(iconOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnReportDate, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReportMounth)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReportWeek)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReportHour)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(iconOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(btnReportDate, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnReportMounth, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnReportWeek, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnReportHour, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(122, 122, 122))
        );

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, 340));

        pnlSearch2.setBackground(new java.awt.Color(63, 54, 53));
        jPanel1.add(pnlSearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 40, 200, 40));

        btnSearch.setBackground(new java.awt.Color(84, 70, 63));
        btnSearch.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("Search");
        btnSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        jPanel1.add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 40, 127, 33));

        pnlSearch1.setBackground(new java.awt.Color(63, 54, 53));
        jPanel1.add(pnlSearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 200, 33));

        jLabel2.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("End - Date");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 40, -1, -1));

        jLabel3.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Start - Date");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(524, 524, 524)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1276, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnReportDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportDateActionPerformed
        scrChart.setViewportView(new ReportDsale());
    }//GEN-LAST:event_btnReportDateActionPerformed

    private void btnReportMounthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportMounthActionPerformed
        scrChart.setViewportView(new ReportMsale());
    }//GEN-LAST:event_btnReportMounthActionPerformed

    private void btnReportWeekActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportWeekActionPerformed
        scrChart.setViewportView(new ReportWsale());
    }//GEN-LAST:event_btnReportWeekActionPerformed

    private void btnReportHourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportHourActionPerformed
        scrChart.setViewportView(new ReportHsale());
    }//GEN-LAST:event_btnReportHourActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        reportList3 = reportService.getReportSaleByMounth(begin, end);
        reportList4 = reportService.getReportSaleByDay(begin, end);
        reportList5 = reportService.getReportSaleByWeek(begin, end);
        reportList6 = reportService.getReportSaleByHour(begin);
        ((AbstractTableModel) tblSaleM.getModel()).fireTableDataChanged();
        ((AbstractTableModel) tblSaleD.getModel()).fireTableDataChanged();
        ((AbstractTableModel) tblSaleW.getModel()).fireTableDataChanged();
        ((AbstractTableModel) tblSaleH.getModel()).fireTableDataChanged();
        scrChart.setViewportView(new ReportLinePanel());

    }//GEN-LAST:event_btnSearchActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReportOrderFrames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReportOrderFrames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReportOrderFrames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReportOrderFrames.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReportOrderFrames().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnReportDate;
    private javax.swing.JButton btnReportHour;
    private javax.swing.JButton btnReportMounth;
    private javax.swing.JButton btnReportWeek;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel iconOrder;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JPanel plChart;
    private javax.swing.JPanel pnlSearch1;
    private javax.swing.JPanel pnlSearch2;
    private javax.swing.JScrollPane scrChart;
    private javax.swing.JTable tblSaleD;
    private javax.swing.JTable tblSaleH;
    private javax.swing.JTable tblSaleM;
    private javax.swing.JTable tblSaleW;
    // End of variables declaration//GEN-END:variables
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui.Report;

import com.Dcoffee.catfish.model.Employee;
import com.Dcoffee.catfish.model.ReportEmployee;
import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportStock;
import com.Dcoffee.catfish.model.ReportThisMouth;
import com.Dcoffee.catfish.model.ReportToday;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import com.Dcoffee.catfish.service.EmployeeService;
import com.Dcoffee.catfish.service.ReportService;
import com.Dcoffee.catfish.service.UserService;
import com.Dcoffee.catfish.ui.DateLabelFormatter;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Miso
 */
public class ReportEmpPanel extends javax.swing.JPanel {

    private final ReportService reportService;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private List<ReportTotalCustomer> reportList;
    private List<ReportThisMouth> reportThisMouth;
    private List<ReportProduct> reportList1;
    private List<ReportStock> reportList2;
    private List<ReportEmployee> reportwork;
    private DefaultPieDataset Piedataset;
    private final int empId;
    private javax.swing.JLabel testLabel;
    private EmployeeService empService;
    private List<Employee> empList;

    /**
     * Creates new form ReportPanel
     */
    public ReportEmpPanel() {
        initComponents();
        empService = new EmployeeService();
        scrChart.setViewportView(new ReportPiePanel());
        btnPie.setIcon(new ImageIcon("img/piechart.png"));
        btnBar.setIcon(new ImageIcon("img/bargraph.png"));
        iconMember.setIcon(new ImageIcon("img/mem.png"));
        iconStock.setIcon(new ImageIcon("img/outstock.png"));
        iconBest.setIcon(new ImageIcon("img/best.png"));
        iconPreformance.setIcon(new ImageIcon("img/work (2).png"));
        reportService = new ReportService();
        reportList1 = reportService.getBestProduct();
        reportList = reportService.getTotalByCus();
        reportList2 = reportService.getOutStock();
        empId = UserService.getCurrentUser().getEmployeeId();
        Employee employee = empService.getEmployeeID(empId);
//        System.out.print(employee);
        txtName.setText(employee.getName() + " " + employee.getSurname());
        lbPhone.setText(employee.getPhone());
        lbAddress.setText(employee.getAddress());
        lbEmail.setText(employee.getEmail());
        reportwork = reportService.getReportEmployee(empId);
        reportThisMouth = reportService.getReportThisMouth(empId);
        ImageIcon icon = new ImageIcon("./img_emp/emp" + employee.getId() + ".png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) (100.0 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        pic.setIcon(icon);
        StringBuilder labelText = new StringBuilder();
        if (reportThisMouth != null && !reportThisMouth.isEmpty()) {
            for (ReportThisMouth report : reportThisMouth) {
                labelText.append(report.getTotalCheckTime());
            }
            lbCurrent.setText(labelText.toString());

        }

//        System.out.println(""+reportList1);
        tblProduct.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblStock.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblCus.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblWorkPerformance.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        initTableMember();
        initTableProduct();
        initTableStock();
        initEmployee();
    }

    private void initTableProduct() {
        tblProduct.setModel(new AbstractTableModel() {
            String[] columnNames = {"Product", "ToTal"};

            @Override

            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList1.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportProduct report = reportList1.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getName();
                    case 1:
                        return report.getTotalAmount();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableMember() {
        tblCus.setModel(new AbstractTableModel() {
            String[] columnNames = {"CustomerId", "CustomerName", "TotalPurchases", "ToTalOrders"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportTotalCustomer report = reportList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getId();
                    case 1:
                        return report.getCusName() + " " + report.getSurName();
                    case 2:
                        return report.getTotalPurchases();
                    case 3:
                        return report.getTatalOrder();
                    default:
                        return "";
                }
            }
        });
//        System.out.println(""+reportList);
//        imgdash.setIcon(new ImageIcon("img/imgdash.png"));
    }

    private void initTableStock() {
        tblStock.setModel(new AbstractTableModel() {
            String[] columnNames = {"MaterialName", "MaterialQty", "MaterialMin"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList2.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportStock report = reportList2.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getMaterialName();
                    case 1:
                        // กำหนดสีเป็นสีแดงสำหรับคอลัมน์ที่ 2 (MaterialQty)
                        cellRenderer.setForeground(Color.RED);
                        return report.getMaterialQty();
                    case 2:
                        return report.getMaterialMin();
                    default:
                        return "";
                }
            }
        });
    }

    private void initEmployee() {
        tblWorkPerformance.setModel(new AbstractTableModel() {
            String[] columnNames = {"In", "Out", "hour","payroll_date", "income"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportwork.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportEmployee report = reportwork.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getTimein();
                    case 1:
                        return report.getTimeout();
                    case 2:
                        return report.getTimehour();
                        case 3:
                        return report.getDate();
                    case 4:
                        return report.getIncome();
                    default:
                        return "";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        iconMember = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCus = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        iconBest = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        btnPie = new javax.swing.JButton();
        btnBar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        scrChart = new javax.swing.JScrollPane();
        plChart = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        iconStock = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        pic = new javax.swing.JLabel();
        lbPhone = new javax.swing.JLabel();
        lbname1 = new javax.swing.JLabel();
        lbname2 = new javax.swing.JLabel();
        IdNumber = new javax.swing.JLabel();
        lbname3 = new javax.swing.JLabel();
        lbname4 = new javax.swing.JLabel();
        lbAddress = new javax.swing.JLabel();
        lbCurrent = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        lbEmail = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblWorkPerformance = new javax.swing.JTable();
        iconPreformance = new javax.swing.JLabel();

        setBackground(new java.awt.Color(65, 55, 54));

        jPanel2.setBackground(new java.awt.Color(211, 197, 186));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel2.setForeground(new java.awt.Color(211, 197, 186));

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel2.setText("Regular Customer");

        iconMember.setForeground(new java.awt.Color(102, 102, 102));

        tblCus.setBackground(new java.awt.Color(211, 197, 186));
        tblCus.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblCus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "CustomerId", "CustomerName", "TotalPurchases", "ToTalOrders"
            }
        ));
        jScrollPane1.setViewportView(tblCus);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 568, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(iconMember, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jLabel2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(iconMember, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(211, 197, 186));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel3.add(iconBest, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 50, 50));

        tblProduct.setBackground(new java.awt.Color(211, 197, 186));
        tblProduct.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane2.setViewportView(tblProduct);

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 310, 100));

        btnPie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPieActionPerformed(evt);
            }
        });
        jPanel3.add(btnPie, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 120, 40));

        btnBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBarActionPerformed(evt);
            }
        });
        jPanel3.add(btnBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 170, 120, 40));

        jLabel6.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel6.setText("Best Seller");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 0, -1, -1));

        jLabel9.setText("__________________________");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 40, -1, -1));

        plChart.setBackground(new java.awt.Color(156, 131, 112));
        plChart.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        plChart.setMaximumSize(new java.awt.Dimension(560, 367));
        plChart.setMinimumSize(new java.awt.Dimension(560, 367));

        javax.swing.GroupLayout plChartLayout = new javax.swing.GroupLayout(plChart);
        plChart.setLayout(plChartLayout);
        plChartLayout.setHorizontalGroup(
            plChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1389, Short.MAX_VALUE)
        );
        plChartLayout.setVerticalGroup(
            plChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1120, Short.MAX_VALUE)
        );

        scrChart.setViewportView(plChart);

        jPanel4.setBackground(new java.awt.Color(211, 197, 186));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jLabel3.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 0, 51));
        jLabel3.setText("Out Stock !");

        tblStock.setBackground(new java.awt.Color(211, 197, 186));
        tblStock.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblStock);

        jLabel4.setFont(new java.awt.Font("MV Boli", 0, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 0, 51));
        jLabel4.setText("Please tell the owner ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(iconStock, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 42, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(iconStock, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, Short.MAX_VALUE)
                .addGap(25, 25, 25))
        );

        jPanel6.setBackground(new java.awt.Color(65, 55, 54));
        jPanel6.setForeground(new java.awt.Color(65, 55, 54));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel7.setBackground(new java.awt.Color(209, 196, 186));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel7.add(pic, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 84, 86));

        lbPhone.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbPhone.setText("test");
        jPanel7.add(lbPhone, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 30, 180, 32));

        lbname1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbname1.setText("Address :");
        jPanel7.add(lbname1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 70, 90, 32));

        lbname2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbname2.setText("Total working this month :");
        jPanel7.add(lbname2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 110, 210, 32));

        IdNumber.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        IdNumber.setText("Tel :");
        jPanel7.add(IdNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 30, 90, 32));

        lbname3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbname3.setText("Gmail  :");
        jPanel7.add(lbname3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 70, 90, 32));

        lbname4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbname4.setText("Name :");
        jPanel7.add(lbname4, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, 90, 32));

        lbAddress.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbAddress.setText("test");
        jPanel7.add(lbAddress, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 70, 140, 32));

        lbCurrent.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbCurrent.setText("test");
        jPanel7.add(lbCurrent, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 110, 90, 32));

        txtName.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtName.setText("test");
        jPanel7.add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 30, 140, 32));

        lbEmail.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbEmail.setText("test");
        jPanel7.add(lbEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, 180, 32));

        jLabel7.setBackground(new java.awt.Color(0, 0, 0));
        jLabel7.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel7.setText("Proflie");
        jPanel7.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, -10, -1, -1));

        jPanel5.setBackground(new java.awt.Color(209, 196, 186));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel5.setForeground(new java.awt.Color(211, 197, 186));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel5.setText("Work Performance Report");
        jPanel5.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 15, 434, 49));

        tblWorkPerformance.setBackground(new java.awt.Color(211, 197, 186));
        tblWorkPerformance.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblWorkPerformance.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "In", "Out", "Payroll_Date", "Income"
            }
        ));
        jScrollPane4.setViewportView(tblWorkPerformance);

        jPanel5.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 568, 105));

        iconPreformance.setForeground(new java.awt.Color(102, 102, 102));
        jPanel5.add(iconPreformance, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 50, 50));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scrChart, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(scrChart, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPieActionPerformed
        scrChart.setViewportView(new ReportPiePanel());

    }//GEN-LAST:event_btnPieActionPerformed

    private void btnBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBarActionPerformed
        scrChart.setViewportView(new ReportBarPanel());
    }//GEN-LAST:event_btnBarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel IdNumber;
    private javax.swing.JButton btnBar;
    private javax.swing.JButton btnPie;
    private javax.swing.JLabel iconBest;
    private javax.swing.JLabel iconMember;
    private javax.swing.JLabel iconPreformance;
    private javax.swing.JLabel iconStock;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lbAddress;
    private javax.swing.JLabel lbCurrent;
    private javax.swing.JLabel lbEmail;
    private javax.swing.JLabel lbPhone;
    private javax.swing.JLabel lbname1;
    private javax.swing.JLabel lbname2;
    private javax.swing.JLabel lbname3;
    private javax.swing.JLabel lbname4;
    private javax.swing.JLabel pic;
    private javax.swing.JPanel plChart;
    private javax.swing.JScrollPane scrChart;
    private javax.swing.JTable tblCus;
    private javax.swing.JTable tblProduct;
    private javax.swing.JTable tblStock;
    private javax.swing.JTable tblWorkPerformance;
    private javax.swing.JLabel txtName;
    // End of variables declaration//GEN-END:variables
}

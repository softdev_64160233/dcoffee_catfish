/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui.Report;

import com.Dcoffee.catfish.model.Order;
import com.Dcoffee.catfish.model.OrderProduct;
import com.Dcoffee.catfish.model.ReportBackMounth;
import com.Dcoffee.catfish.model.ReportImportLast;
import com.Dcoffee.catfish.model.ReportPayroll;
import com.Dcoffee.catfish.model.ReportProductBack;
import com.Dcoffee.catfish.model.ReportQty;
import com.Dcoffee.catfish.model.ReportTime;
import com.Dcoffee.catfish.model.ReportToday;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import com.Dcoffee.catfish.service.ReportService;
import com.Dcoffee.catfish.ui.StockCheckDialog;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Miso
 */
public class ReportMgPanel extends javax.swing.JPanel {

    private final ReportService reportService;
    private List<ReportTime> reportList;
    private List<ReportToday> reportList1;
    private List<ReportQty> reportList2;
    private List<ReportQty> reportList3;
    private List<ReportToday> reportList4;
    private Order editedOrder;
    private OrderProduct editedOrderPro;

    private List<com.Dcoffee.catfish.model.ReportUtilitys> reportUtility;
    private List<ReportBackMounth> reportSumback;
    private List<ReportPayroll> reportPayroll;
    private List<ReportTotalCustomer> reportListMounth;
    private List<ReportProductBack> reportProductBack;
    private List<ReportImportLast> reportImportLast;

    /**
     * Creates new form ReportMgPanel
     */
    public ReportMgPanel() {
        initComponents();

        ImageIcon gifIcon = new ImageIcon("img/presen.gif");
        txtHeaderD.setIcon(new ImageIcon("img/daily.png"));
        txtHeader.setIcon(new ImageIcon("img/time.png"));
        iconDatePanel.setIcon(new ImageIcon("img/th.png"));
        iconPanelSell.setIcon(new ImageIcon("img/mb16.png"));
        iconPanelOrder.setIcon(new ImageIcon("img/cc16.png"));
        iconPanelAmount.setIcon(new ImageIcon("img/cf.png"));
        imgCoffee.setIcon(new ImageIcon("img/coffee.png"));
        txtPro.setIcon(new ImageIcon("img/star.png"));
        txtCus.setIcon(new ImageIcon("img/star.png"));
        btnView.setIcon(new ImageIcon("img/view.png"));
        iconMonth.setIcon(new ImageIcon("img/month.png"));
        iconB1.setIcon(new ImageIcon("img/baht.png"));
        iconB2.setIcon(new ImageIcon("img/baht.png"));
        iconB3.setIcon(new ImageIcon("img/baht.png"));
        iconB4.setIcon(new ImageIcon("img/baht.png"));
        iconB5.setIcon(new ImageIcon("img/baht.png"));
        reportService = new ReportService();
        reportList = reportService.getCheckEmp();
        reportList1 = reportService.getReportToday();
        reportList4 = reportService.getYesterdayReport();
        reportList2 = reportService.getTodayReportQty();
        reportList3 = reportService.getYesterdayReportQty();
        tblCheck.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tbkProductBack.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblCus.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));

        StringBuilder labelText = new StringBuilder();
        initTableEmp();
        initDaily();
        initQty();
        CalReportQty();
        initUtilityBack();
        initDiscount();
        initSumback();
        initPayroll();
        initTableMember();
        initTabletbkProduct();
        initImportLast();
        double Saletoday = 0;
        double Saleyesterday = 0;
        double totalSale = 0;
        int OrderToday = 0;
        int OrderYesterday = 0;
        double totalOrder = 0;

        if (reportList1 != null && !reportList1.isEmpty()) {
            for (ReportToday report : reportList1) {
                Saletoday += report.getTotalSales();
                OrderToday += report.getTotalorders();
            }
        }
        if (reportList4 != null && !reportList4.isEmpty()) {
            for (ReportToday report : reportList4) {
                Saleyesterday += report.getTotalSales();
                OrderYesterday += report.getTotalorders();
            }
        }

        totalSale = Saletoday - Saleyesterday;
        totalSale = totalSale / Saleyesterday;
        totalSale = totalSale * 100;
        String formattedPercentageChange = String.format("%.2f", totalSale);

        if (totalSale < 0) {
            txtTotalSales.setText(String.valueOf(formattedPercentageChange) + " " + "%" + " VS Time Yesterday");
            iconTS.setIcon(new ImageIcon("img/down.png"));
        } else if (totalSale == 0) {
            txtTotalSales.setText(formattedPercentageChange + "% VS Time Yesterday");
            //            iconUD.setIcon(new ImageIcon("img/down.png"));
        } else {
            txtTotalSales.setText(String.valueOf(formattedPercentageChange) + " " + "%" + " VS Time Yesterday");
            iconTS.setIcon(new ImageIcon("img/up.png"));
        }
        totalOrder = OrderToday - OrderYesterday;
        totalOrder = totalOrder / OrderYesterday;
        totalOrder = totalOrder * 100;
        if (totalOrder < 0) {
            txtOrderToDay.setText(String.valueOf(totalOrder) + " " + "%" + " VS Time Yesterday");
            iconOD.setIcon(new ImageIcon("img/down.png"));
        } else if (totalOrder == 0) {
            txtOrderToDay.setText(String.valueOf(totalOrder) + " " + "%" + " VS Time Yesterday");
            //            iconUD.setIcon(new ImageIcon("img/down.png"));
        } else {
            txtOrderToDay.setText(String.valueOf(totalOrder) + " " + "%" + " VS Time Yesterday");
            iconOD.setIcon(new ImageIcon("img/up.png"));
        }

    }

    private void CalReportQty() {
        reportList3 = reportService.getYesterdayReportQty();
        int sum = 0;
        int sumyesterday = 0;
        double total = 0;

        for (ReportQty report : reportList2) {
            sum += report.getAmount();
        }

        for (ReportQty report : reportList3) {
            sumyesterday += report.getAmount();
        }

        total = sum - sumyesterday;
        System.out.print(total);
        total = total / sumyesterday;
        total = total * 100;
        if (total < 0) {
            txtUnitsToDay.setText(String.format("%.2f", total) + " %" + "VS Time Yesterday");

            iconUD.setIcon(new ImageIcon("img/down.png"));
        } else if (total == 0) {
            txtUnitsToDay.setText(String.valueOf(total) + " " + "%" + " VS Time Yesterday");
//            iconUD.setIcon(new ImageIcon("img/down.png"));
        } else {
            txtUnitsToDay.setText(String.format("%.2f", total) + " %" + "VS Time Yesterday");

            iconUD.setIcon(new ImageIcon("img/up.png"));
        }
    }

    private void initQty() {
        reportList3 = reportService.getYesterdayReportQty();
        int sum = 0;
        for (ReportQty report : reportList2) {
            sum += report.getAmount();
        }
        txtamount.setText(String.valueOf(sum));
        System.out.println("ผลรวมทั้งหมด: " + sum);
    }

    private void initDaily() {
        reportList1 = reportService.getReportToday();
        StringBuilder labelText = new StringBuilder();
        if (reportList1 != null && !reportList1.isEmpty()) {
            for (ReportToday report : reportList1) {
                labelText.append(report.getTotalSales());
            }
            txtNow.setText(labelText.toString());
            labelText.setLength(0);
            for (ReportToday report : reportList1) {
                labelText.append(report.getTotalorders());
            }
            txtOrder.setText(labelText.toString());
        } else {
            txtNow.setText("no info");
            txtOrder.setText("no info");

        }
    }

    private void initDiscount() {
        reportSumback = reportService.getReportBackMounth();
        StringBuilder labelText = new StringBuilder();
        if (reportSumback != null && !reportSumback.isEmpty()) {
            for (ReportBackMounth report : reportSumback) {
                labelText.append(report.getDiscountSum());
            }
            txtDiscount.setText(labelText.toString());

        } else {
            txtDiscount.setText("no info");
        }
    }

    private void initTableEmp() {
        tblCheck.setModel(new AbstractTableModel() {
            String[] columnNames = {"EmpId", "Name", "Attendance_Status", "In", "Out", "Hour"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportTime report = reportList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getEmployeeId();
                    case 1:
                        return report.getUserName();
                    case 2:
                        return report.getAttendanceStatus();
                    case 3:
                        return report.getCheckIntime();
                    case 4:
                        return report.getCheckInOut();
                    case 5:
                        return report.getTotalwork();

                    default:
                        return "";
                }
            }
        });
//        System.out.println(""+reportList);
//        imgdash.setIcon(new ImageIcon("img/imgdash.png"));
    }

    //Mounth
    private void initTableMember() {
        reportListMounth = reportService.getTotalByCusBack();
        tblCus.setModel(new AbstractTableModel() {
            String[] columnNames = {"Name", "TotalPurchases", "ToTalOrders"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportListMounth.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportTotalCustomer report = reportListMounth.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getCusName() + " " + report.getSurName();
                    case 1:
                        return report.getTotalPurchases();
                    case 2:
                        return report.getTatalOrder();
                    default:
                        return "";
                }
            }
        });
//        System.out.println(""+reportList);
//        imgdash.setIcon(new ImageIcon("img/imgdash.png"));
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void initTabletbkProduct() {
        reportProductBack = reportService.getReportProductBack();
        tbkProductBack.setModel(new AbstractTableModel() {
            String[] columnNames = {"ProductName", "Qty", "TotalPurchases"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportProductBack.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportProductBack report = reportProductBack.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getProductName();
                    case 1:
                        return report.getTotalSold();
                    case 2:
                        return report.getPrice();

                    default:
                        return "";
                }
            }
        });
//        System.out.println(""+reportList);
//        imgdash.setIcon(new ImageIcon("img/imgdash.png"));
    }

    private void initPayroll() {
        reportPayroll = reportService.getReportPayroll();
        StringBuilder labelText = new StringBuilder();
        if (reportPayroll != null && !reportPayroll.isEmpty()) {
            for (ReportPayroll report : reportPayroll) {
                labelText.append(report.getTotalPayment());
            }
            txtPayroll.setText(labelText.toString());
            labelText.setLength(0);

        } else {
            txtPayroll.setText("ไม่มีข้อมูลใน reportList1");

        }
    }

    private void initSumback() {
        reportSumback = reportService.getReportBackMounth();
        StringBuilder labelText = new StringBuilder();
        if (reportSumback != null && !reportSumback.isEmpty()) {
            for (ReportBackMounth report : reportSumback) {
                labelText.append(report.getTotalSum());
            }
            txtSumback.setText(labelText.toString());
            labelText.setLength(0);

        } else {
            txtSumback.setText("ไม่มีข้อมูลใน reportList1");

        }
    }

    private void initUtilityBack() {
        reportUtility = reportService.getReportUtilitys();
        StringBuilder labelText = new StringBuilder();
        if (reportUtility != null && !reportUtility.isEmpty()) {
            for (com.Dcoffee.catfish.model.ReportUtilitys report : reportUtility) {
                labelText.append(report.getTotal());
            }
            txtUtility.setText(labelText.toString());
            labelText.setLength(0);


        } else {
            txtUtility.setText("no info");

        }
    }

    private void initImportLast() {
        reportImportLast = reportService.getReportImportLast();
        StringBuilder labelText = new StringBuilder();
        if (reportImportLast != null && !reportImportLast.isEmpty()) {
            for (ReportImportLast report : reportImportLast) {
                labelText.append(report.getTotalImportForLastMonth());
            }
            txtImport.setText(labelText.toString());
        } else {
            txtImport.setText("No info");

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel15 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtNow = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        iconDatePanel = new javax.swing.JLabel();
        iconPanelSell = new javax.swing.JLabel();
        iconTS = new javax.swing.JLabel();
        txtTotalSales = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtOrder = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        iconPanelOrder = new javax.swing.JLabel();
        iconOD = new javax.swing.JLabel();
        txtOrderToDay = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtHeader = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCheck = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        iconDay = new javax.swing.JLabel();
        txtHeaderD = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtamount = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        iconPanelAmount = new javax.swing.JLabel();
        btnView = new javax.swing.JButton();
        iconUD = new javax.swing.JLabel();
        txtUnitsToDay = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        txtUtility = new javax.swing.JLabel();
        iconB5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txtImport = new javax.swing.JLabel();
        iconB4 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txtSumback = new javax.swing.JLabel();
        iconB2 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        txtPayroll = new javax.swing.JLabel();
        iconB1 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCus = new javax.swing.JTable();
        txtCus = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbkProductBack = new javax.swing.JTable();
        txtPro = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JLabel();
        iconB3 = new javax.swing.JLabel();
        iconMonth = new javax.swing.JLabel();
        imgCoffee = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setBackground(new java.awt.Color(65, 55, 54));

        jPanel5.setBackground(new java.awt.Color(211, 197, 186));

        jPanel2.setBackground(new java.awt.Color(65, 55, 54));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Angsana New", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("TOTAL SALES ");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, -1, -1));

        txtNow.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 36)); // NOI18N
        txtNow.setForeground(new java.awt.Color(0, 153, 51));
        txtNow.setText("2000.0");
        jPanel2.add(txtNow, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, -1, 37));
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, -1, 4));

        iconDatePanel.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jPanel2.add(iconDatePanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, 30, 40));
        jPanel2.add(iconPanelSell, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, 22, 24));

        iconTS.setFont(new java.awt.Font("Angsana New", 0, 14)); // NOI18N
        iconTS.setForeground(new java.awt.Color(255, 255, 255));
        iconTS.setText("i");
        jPanel2.add(iconTS, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 15, -1));

        txtTotalSales.setBackground(new java.awt.Color(51, 51, 51));
        txtTotalSales.setFont(new java.awt.Font("Arial Black", 0, 10)); // NOI18N
        txtTotalSales.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.add(txtTotalSales, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 160, 16));

        jPanel3.setBackground(new java.awt.Color(65, 55, 54));
        jPanel3.setPreferredSize(new java.awt.Dimension(230, 136));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setFont(new java.awt.Font("Angsana New", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("TOTAL ORDER TODAY");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 80, -1, -1));

        txtOrder.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 48)); // NOI18N
        txtOrder.setForeground(new java.awt.Color(0, 153, 51));
        txtOrder.setText("num");
        jPanel3.add(txtOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 110, 41));
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 144, -1, -1));
        jPanel3.add(iconPanelOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 25, 26));

        iconOD.setFont(new java.awt.Font("Angsana New", 0, 14)); // NOI18N
        iconOD.setForeground(new java.awt.Color(255, 255, 255));
        iconOD.setText("i");
        jPanel3.add(iconOD, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 113, 15, -1));

        txtOrderToDay.setBackground(new java.awt.Color(51, 51, 51));
        txtOrderToDay.setFont(new java.awt.Font("Arial Black", 0, 10)); // NOI18N
        txtOrderToDay.setForeground(new java.awt.Color(255, 255, 255));
        jPanel3.add(txtOrderToDay, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 113, 193, 16));

        jPanel1.setBackground(new java.awt.Color(211, 197, 186));

        txtHeader.setBackground(new java.awt.Color(187, 187, 187));
        txtHeader.setFont(new java.awt.Font("Angsana New", 1, 36)); // NOI18N
        txtHeader.setText("Employee Attendance Report for Today");

        tblCheck.setBackground(new java.awt.Color(65, 55, 54));
        tblCheck.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblCheck.setForeground(new java.awt.Color(255, 255, 255));
        tblCheck.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblCheck);

        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("---------------------------------------------------------------------------------------------");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addComponent(txtHeader)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 93, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 486, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(txtHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        iconDay.setBackground(new java.awt.Color(102, 255, 51));
        iconDay.setForeground(new java.awt.Color(255, 255, 255));

        txtHeaderD.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        txtHeaderD.setText("Daily Totals");

        jPanel4.setBackground(new java.awt.Color(65, 55, 54));
        jPanel4.setPreferredSize(new java.awt.Dimension(230, 136));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel10.setFont(new java.awt.Font("Angsana New", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("TOTAL UNITS TODAY");
        jPanel4.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, -1, -1));

        txtamount.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 48)); // NOI18N
        txtamount.setForeground(new java.awt.Color(0, 153, 51));
        txtamount.setText("num");
        jPanel4.add(txtamount, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, 110, 40));
        jPanel4.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 6, -1, 138));
        jPanel4.add(iconPanelAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 21, 21));

        btnView.setBackground(new java.awt.Color(84, 70, 63));
        btnView.setFont(new java.awt.Font("Angsana New", 1, 14)); // NOI18N
        btnView.setForeground(new java.awt.Color(255, 255, 255));
        btnView.setText("View");
        btnView.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });
        jPanel4.add(btnView, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 70, -1));

        iconUD.setFont(new java.awt.Font("Angsana New", 0, 14)); // NOI18N
        iconUD.setForeground(new java.awt.Color(255, 255, 255));
        iconUD.setText("i");
        jPanel4.add(iconUD, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 15, -1));

        txtUnitsToDay.setBackground(new java.awt.Color(51, 51, 51));
        txtUnitsToDay.setFont(new java.awt.Font("Arial Black", 0, 10)); // NOI18N
        txtUnitsToDay.setForeground(new java.awt.Color(255, 255, 255));
        jPanel4.add(txtUnitsToDay, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 197, 16));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(81, 81, 81))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                                .addComponent(iconDay, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtHeaderD, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(250, 250, 250))))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(22, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(iconDay, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtHeaderD, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );

        jPanel6.setBackground(new java.awt.Color(211, 197, 186));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel7.setBackground(new java.awt.Color(65, 55, 54));
        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel18.setBackground(new java.awt.Color(255, 255, 255));
        jLabel18.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Utility Expense");
        jPanel7.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 10, 110, 31));

        txtUtility.setBackground(new java.awt.Color(255, 255, 255));
        txtUtility.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 36)); // NOI18N
        txtUtility.setForeground(new java.awt.Color(255, 51, 51));
        txtUtility.setText("Utility");
        jPanel7.add(txtUtility, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 120, -1));
        jPanel7.add(iconB5, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 20, 20));

        jPanel6.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 69, 180, 100));

        jPanel8.setBackground(new java.awt.Color(65, 55, 54));
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText(" Import Expenses");
        jPanel8.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 10, -1, -1));

        txtImport.setBackground(new java.awt.Color(255, 255, 255));
        txtImport.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 36)); // NOI18N
        txtImport.setForeground(new java.awt.Color(255, 51, 51));
        txtImport.setText("import");
        jPanel8.add(txtImport, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, -1, -1));

        iconB4.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jPanel8.add(iconB4, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 20, 20));

        jPanel6.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(478, 69, 180, 100));

        jPanel9.setBackground(new java.awt.Color(65, 55, 54));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Total income this now");
        jPanel9.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        txtSumback.setBackground(new java.awt.Color(255, 255, 255));
        txtSumback.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 36)); // NOI18N
        txtSumback.setForeground(new java.awt.Color(0, 153, 51));
        txtSumback.setText("SumBack");
        jPanel9.add(txtSumback, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, -1, -1));

        iconB2.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jPanel9.add(iconB2, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 20, 20));

        jPanel6.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 194, 180, 86));

        jPanel10.setBackground(new java.awt.Color(65, 55, 54));
        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel17.setBackground(new java.awt.Color(255, 255, 255));
        jLabel17.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText(" Payroll Expenses ");
        jPanel10.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, -1));

        txtPayroll.setBackground(new java.awt.Color(255, 255, 255));
        txtPayroll.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 36)); // NOI18N
        txtPayroll.setForeground(new java.awt.Color(255, 51, 51));
        txtPayroll.setText("Payroll");
        jPanel10.add(txtPayroll, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 40, -1, -1));

        iconB1.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jPanel10.add(iconB1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 20, 20));

        jPanel6.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(255, 194, 180, 86));

        jPanel12.setBackground(new java.awt.Color(65, 55, 54));

        tblCus.setBackground(new java.awt.Color(65, 55, 54));
        tblCus.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblCus.setForeground(new java.awt.Color(255, 255, 255));
        tblCus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblCus);

        txtCus.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        txtCus.setForeground(new java.awt.Color(255, 255, 255));
        txtCus.setText("Regular Customer");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(txtCus))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtCus, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(12, 12, 12))
        );

        jPanel6.add(jPanel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 298, -1, 301));

        jPanel13.setBackground(new java.awt.Color(65, 55, 54));

        tbkProductBack.setBackground(new java.awt.Color(65, 55, 54));
        tbkProductBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbkProductBack.setForeground(new java.awt.Color(255, 255, 255));
        tbkProductBack.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbkProductBack);

        txtPro.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        txtPro.setForeground(new java.awt.Color(255, 255, 255));
        txtPro.setText("Poppular Products");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(txtPro)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addComponent(txtPro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel6.add(jPanel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(384, 298, 280, -1));

        jLabel12.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        jLabel12.setText("Mounthy Totals");
        jPanel6.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(269, 10, 198, 33));

        jPanel11.setBackground(new java.awt.Color(65, 55, 54));
        jPanel11.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel19.setBackground(new java.awt.Color(255, 255, 255));
        jLabel19.setFont(new java.awt.Font("Angsana New", 1, 24)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Discount Order");
        jPanel11.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, -1, -1));

        txtDiscount.setBackground(new java.awt.Color(255, 255, 255));
        txtDiscount.setFont(new java.awt.Font("Microsoft Yi Baiti", 1, 36)); // NOI18N
        txtDiscount.setForeground(new java.awt.Color(255, 51, 51));
        txtDiscount.setText("discount");
        jPanel11.add(txtDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, -1, -1));

        iconB3.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jPanel11.add(iconB3, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 20, 20));

        jPanel6.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(255, 69, 180, 100));

        iconMonth.setBackground(new java.awt.Color(102, 255, 51));
        iconMonth.setForeground(new java.awt.Color(255, 255, 255));
        jPanel6.add(iconMonth, new org.netbeans.lib.awtextra.AbsoluteConstraints(223, 15, 28, 28));
        jPanel6.add(imgCoffee, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 180, 130, 110));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(15, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        ReportViewDay report = new ReportViewDay();
        report.setResizable(false);
        report.setLocationRelativeTo(null);
        report.setMinimumSize(null);
        report.setVisible(true);
    }//GEN-LAST:event_btnViewActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnView;
    private javax.swing.JLabel iconB1;
    private javax.swing.JLabel iconB2;
    private javax.swing.JLabel iconB3;
    private javax.swing.JLabel iconB4;
    private javax.swing.JLabel iconB5;
    private javax.swing.JLabel iconDatePanel;
    private javax.swing.JLabel iconDay;
    private javax.swing.JLabel iconMonth;
    private javax.swing.JLabel iconOD;
    private javax.swing.JLabel iconPanelAmount;
    private javax.swing.JLabel iconPanelOrder;
    private javax.swing.JLabel iconPanelSell;
    private javax.swing.JLabel iconTS;
    private javax.swing.JLabel iconUD;
    private javax.swing.JLabel imgCoffee;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tbkProductBack;
    private javax.swing.JTable tblCheck;
    private javax.swing.JTable tblCus;
    private javax.swing.JLabel txtCus;
    private javax.swing.JLabel txtDiscount;
    private javax.swing.JLabel txtHeader;
    private javax.swing.JLabel txtHeaderD;
    private javax.swing.JLabel txtImport;
    private javax.swing.JLabel txtNow;
    private javax.swing.JLabel txtOrder;
    private javax.swing.JLabel txtOrderToDay;
    private javax.swing.JLabel txtPayroll;
    private javax.swing.JLabel txtPro;
    private javax.swing.JLabel txtSumback;
    private javax.swing.JLabel txtTotalSales;
    private javax.swing.JLabel txtUnitsToDay;
    private javax.swing.JLabel txtUtility;
    private javax.swing.JLabel txtamount;
    // End of variables declaration//GEN-END:variables

}

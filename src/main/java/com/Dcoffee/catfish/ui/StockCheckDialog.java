/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.CheckStoreDetail;
import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.service.CheckStoreService;
import com.Dcoffee.catfish.service.MaterialService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author kantinan
 */
public class StockCheckDialog extends javax.swing.JDialog {

    private final CheckStoreService checkStoreService;
    private CheckStore editedCheckStore;
    CheckStore checkStore;
    private List<CheckStore> checkStoreList;
    CheckStoreDetail checkStoreDetail;
    private final MaterialService mtrService;
    private List<Material> mtrList;
    private ArrayList<CheckStoreDetail> checkStoreDetails = new ArrayList<CheckStoreDetail>();

    /**
     * Creates new form StockCheckDialog
     */
    public StockCheckDialog(java.awt.Frame parent, CheckStore editedCheckStore) {
        super(parent, true);
        this.setUndecorated(true);
        initComponents();
        btnDelete.setIcon(new ImageIcon("img/deleteW.png"));
        btnCheck.setIcon(new ImageIcon("img/addW.png"));
        btnClear.setIcon(new ImageIcon("img/clearW.png"));
        checkStore = new CheckStore();
        checkStore.setUserId(UserService.getCurrentUser().getId());
        this.editedCheckStore = editedCheckStore;

        checkStoreService = new CheckStoreService();
        checkStoreList = checkStoreService.getCheckStores();
        mtrService = new MaterialService();
        mtrList = mtrService.getMaterials();
//        mtrList = mtrService.getMaterialsOrderByName();

        tblMat.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblCheck.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "NAME",
                "MINIMUM", "QTY", "UNIT"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return mtrList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material mtr = mtrList.get(rowIndex);
                switch (columnIndex) {

                    case 0:
                        return mtr.getMaterialName();
                    case 1:
                        return mtr.getMaterialMin();
                    case 2:
                        return mtr.getMaterialQty();
                    case 3:
                        return mtr.getMaterialUnit();

                    default:
                        return "Unknowm";
                }
            }
        });

        tblCheck.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Qty"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return checkStore.getCheckStoreDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
                CheckStoreDetail checkStoreDetail = checkStoreDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkStoreDetail.getMaterialName();
                    case 1:
                        return checkStoreDetail.getMaterialQty();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
                CheckStoreDetail checkStoreDetail = checkStoreDetails.get(rowIndex);
                if (columnIndex == 1) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    checkStoreDetail.setMaterialQty(qty);
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 1:

                        return true;
                    default:
                        return false;
                }
            }

        });

        tblMat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblMat.rowAtPoint(e.getPoint());
                int col = tblMat.columnAtPoint(e.getPoint());
                Material material = mtrList.get(row);

                for (CheckStoreDetail detail : checkStore.getCheckStoreDetail()) {
                    if (detail.getMaterialId() == material.getMaterialID()) {
                        return;
                    }
                }
                checkStore.addCheckStoreDetail(material, material.getMaterialQty());
                tblCheck.revalidate();
                tblCheck.repaint();
            }
        });
    }

    public int calculateTotal(ArrayList<CheckStoreDetail> details) {
        int total = 0;
        for (CheckStoreDetail detail : details) {
//        total += detail * detail.getMaterialQty();
        }
        return total;
    }

//    private void refreshRecipt() {
//        tblCheck.revalidate();
//        tblCheck.repaint();
//        lblTotal.setText("Total: " + checkStore.get());
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCheck = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMat = new javax.swing.JTable();
        btnCheck = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(65, 55, 54));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblCheck.setBackground(new java.awt.Color(211, 197, 186));
        tblCheck.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblCheck.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCheck.setRowHeight(40);
        jScrollPane2.setViewportView(tblCheck);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 140, 500, 503));

        tblMat.setBackground(new java.awt.Color(211, 197, 186));
        tblMat.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblMat.setRowHeight(40);
        tblMat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMatMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblMat);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 500, 503));

        btnCheck.setBackground(new java.awt.Color(84, 70, 63));
        btnCheck.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnCheck.setForeground(new java.awt.Color(255, 255, 255));
        btnCheck.setText("Check");
        btnCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckActionPerformed(evt);
            }
        });
        jPanel1.add(btnCheck, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 80, 100, 37));

        jPanel2.setBackground(new java.awt.Color(84, 70, 63));
        jPanel2.setForeground(new java.awt.Color(84, 70, 63));

        jLabel1.setFont(new java.awt.Font("Angsana New", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("        CheckStore");

        jButton1.setBackground(new java.awt.Color(84, 70, 63));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("X");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(424, 424, 424)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 380, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 6, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1)))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1080, -1));

        btnDelete.setBackground(new java.awt.Color(84, 30, 0));
        btnDelete.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(255, 255, 255));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        jPanel1.add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 80, 100, 37));

        btnClear.setBackground(new java.awt.Color(84, 50, 0));
        btnClear.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnClear.setForeground(new java.awt.Color(255, 255, 255));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });
        jPanel1.add(btnClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 80, 100, 37));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckActionPerformed
        System.out.println("" + checkStore);
        checkStoreService.addNew(checkStore);
        this.dispose();

    }//GEN-LAST:event_btnCheckActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedDel = tblCheck.getSelectedRow();
        ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
        CheckStoreDetail check = checkStoreDetails.get(selectedDel);
        if (selectedDel >= 0) {
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete " + check.getMaterialName() + "?", "Caution!!", JOptionPane.YES_NO_OPTION,
                    JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                checkStore.delStockDetail(check);
            }
            refreshStock();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
        int input = JOptionPane.showConfirmDialog(this,
                "Do you want to clear ?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (input == 0) {
            checkStoreDetails.clear();

        }
        refreshStock();
    }//GEN-LAST:event_btnClearActionPerformed

    private void tblMatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMatMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tblMatMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheck;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCheck;
    private javax.swing.JTable tblMat;
    // End of variables declaration//GEN-END:variables

    private void refreshStock() {
        tblCheck.revalidate();
        tblCheck.repaint();
    }

//    private void updateTableWithFilter(List<Material> filteredList) {
//     tblMat.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
//        tblMat.setModel(new AbstractTableModel() {
//            String[] columnNames = {"ID", "NAME",
//                "MINIMUM", "QTY", "UNIT"};
//
//            @Override
//            public String getColumnName(int column) {
//                return columnNames[column];
//            }
//
//            @Override
//            public int getRowCount() {
//                return mtrList.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 4;
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                Material mtr = mtrList.get(rowIndex);
//                switch (columnIndex) {
//
//                    case 0:
//                        return mtr.getMaterialName();
//                    case 1:
//                        return mtr.getMaterialMin();
//                    case 2:
//                        return mtr.getMaterialQty();
//                    case 3:
//                        return mtr.getMaterialUnit();
//
//                    default:
//                        return "Unknowm";
//                }
//            }
//        });}

}

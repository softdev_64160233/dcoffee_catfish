/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.dao.StockImportDao;
import com.Dcoffee.catfish.model.Bill;
import com.Dcoffee.catfish.model.BillDetail;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.service.CheckStoreService;
import com.Dcoffee.catfish.service.MaterialService;
import com.Dcoffee.catfish.service.StockImportDetailService;
import com.Dcoffee.catfish.service.StockImportService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author User
 */
public class StockImportP extends javax.swing.JPanel {
    //private List<StockImport> stockImport;
    //com.Dcoffee.catfish.model.StockImport stockImport;
    //private StockImportDetail editedStockImportDetail;

//    private final JScrollPane scrMain;
    ArrayList<StockImport> inList;
    private List<StockImportDetail> stockImportList;
    private List<StockImport> StockImportList;
    private final StockImportDetailService stockImportDetailService;
    StockImportService stockImportService = new StockImportService();
    private AbstractTableModel tblImportModel;
    StockImportDao stockImportDao = new StockImportDao();
    UserService userService = new UserService();
    StockImport stockImport;
    Material material;
    MaterialService materialService;
    private AbstractTableModel tbHisModel;
    private final UtilDateModel model2;
    private StockImport editedStockImport;
    DefaultTableModel model;
    

    /**
     * Creates new form StockImport
     */
//    StockImport stockImport;
    public StockImportP() {
        initComponents();
        stockImport = new StockImport(); 
        btnSearch.setIcon(new ImageIcon("img/search.png"));
         model2 = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.mouth", "Mouth");
        p.put("text.year", "Year");
        JDatePanelImpl dataPanel = new JDatePanelImpl(model2, p);
        JDatePickerImpl dataPicker = new JDatePickerImpl(dataPanel, new DateLabelFormatter());
//        pnlDate.setPreferredSize(new Dimension(300, 200));
        pnlDate.add(dataPicker);
        model2.setSelected(true);
        initHisTable();
        tblImport.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblMaterial.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        stockImportDetailService = new StockImportDetailService();
        stockImportDao = new StockImportDao();
        stockImportService = new StockImportService();
        userService = new UserService();
        btnAdd.setIcon(new ImageIcon("img/operation.png"));

        stockImport = new StockImport();
        stockImportList = stockImportDetailService.getHistoryInvoiceDetails();
        tblImport.setRowHeight(30);
        tblImportModel = new AbstractTableModel() {
            String[] headers = {"Name", "Company", "Price", "qty", "total"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return stockImportList.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                //ArrayList<StockImportDetail> stockImportDetails = stockImportList.get();
                StockImportDetail stockImportDetail = stockImportList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return stockImportDetail.getMaterialName();
                    case 1:
                        return stockImportDetail.getStockImportDetailCompany();
                    case 2:
                        return stockImportDetail.getMaterialPrice();
                    case 3:
                        return stockImportDetail.getMaterialQty();
                    case 4:
                        return stockImportDetail.getMaterialTotalPrice();
                    default:
                        return "";
                }
            }

        };

    }
//

    private void initHisTable() {
        inList = stockImportService.getStockImport();
        tblMaterial.setRowHeight(30);
        tbHisModel = new AbstractTableModel() {
            String[] hishead = { "Name", "Date", "total"};

            @Override
            public String getColumnName(int column) {
                return hishead[column];
            }

            @Override
            public int getRowCount() {
                return inList.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                StockImport stockImport = inList.get(rowIndex);
                switch (columnIndex) {

                    case 0:
                        return stockImport.getUser().getName();
//                        return stockImport.getUserId();
                    case 1:
                        return stockImport.getCreatedDate();
                    case 2:
                        return stockImport.getStockImportTotal();
                    default:
                        return "";

//                    case 0:
//                        return stockImport.getMaterialName();
//                    case 1:
//                        return stockImport.getMaterialPrice();
//                    case 2:
//                        return stockImport.getMaterialQty();
//                    case 3:
//                        return stockImport.getMaterialTotalPrice();
                }
            }
        };
        tblMaterial.setModel(tbHisModel);
    }



    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    //@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblMaterial = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblImport = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        pnlDate = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();

        setBackground(new java.awt.Color(65, 55, 54));

        tblMaterial.setBackground(new java.awt.Color(211, 197, 186));
        tblMaterial.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblMaterialMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblMaterial);

        tblImport.setBackground(new java.awt.Color(211, 197, 186));
        tblImport.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblImport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Name", "Company", "Price", "Qty", "Total"
            }
        ));
        jScrollPane2.setViewportView(tblImport);

        btnAdd.setBackground(new java.awt.Color(84, 70, 63));
        btnAdd.setFont(new java.awt.Font("MV Boli", 1, 18)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(255, 255, 255));
        btnAdd.setText("Process");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("AngsanaUPC", 1, 48)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Import History");

        jLabel7.setFont(new java.awt.Font("AngsanaUPC", 1, 48)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Import History - Detail");

        jLabel1.setFont(new java.awt.Font("MV Boli", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Select Day");

        pnlDate.setBackground(new java.awt.Color(65, 55, 54));

        btnSearch.setBackground(new java.awt.Color(84, 70, 63));
        btnSearch.setFont(new java.awt.Font("MV Boli", 1, 18)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 962, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(pnlDate, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAdd)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearch))
                    .addComponent(pnlDate, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tblMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblMaterialMouseClicked
        int selectedIndex = tblMaterial.getSelectedRow();
        if (selectedIndex >= 0) {
            StockImport st = inList.get(selectedIndex);
            stockImportList = stockImportDetailService.getHistoryInvoiceDetailsById(st.getId());
            tblImport.setModel(tblImportModel);
            refreshHisTable(st.getId());

        }
    }//GEN-LAST:event_tblMaterialMouseClicked

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
//        scrMain.setViewportView(new AddStockImport(scrMain));
        editedStockImport = new StockImport();
        openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
//        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
//        //        System.out.println(""+date.format(model.getValue()));
//        String datetime = date.format(model2.getValue());
//        inList = (ArrayList<StockImport>) stockImportService.getDate(datetime);
//        ((AbstractTableModel) tblMaterial.getModel()).fireTableDataChanged();

    }//GEN-LAST:event_btnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlDate;
    private javax.swing.JTable tblImport;
    private javax.swing.JTable tblMaterial;
    // End of variables declaration//GEN-END:variables

    private void enablefrom(boolean status) {
        DefaultTableModel model = new DefaultTableModel();
        this.model = model; // กำหนดค่าให้กับ this.model

        if (status == false) {
            model.setRowCount(0);
            //lblSum.setText("Sum: 0.00");
        }
        //lblSum.setEnabled(status);
//        btnCancel.setEnabled(status);
        //btnSave.setEnabled(status);
        tblMaterial.setEnabled(status);
        tblImport.setEnabled(status);
    }

    private boolean isTableisEmpty() {
        return model.getRowCount() <= 0;
    }

    private void clear() {
        DefaultTableModel model = new DefaultTableModel();
        tblImport.setModel(model);
    }

    private void refreshHisTable(int id) {
        stockImportList = stockImportDetailService.getHistoryInvoiceDetailsById(id);
//        lblTotal.setText("Total: " + hisInvoice.getTotal());
        tblImport.revalidate();
        tblImport.repaint();
        tblMaterial.repaint();
        tblMaterial.revalidate();
    }
    
    public void refreshTable() {
        inList = stockImportService.getStockImport();
        tblMaterial.revalidate();
        tblMaterial.repaint();
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        AddStockImportDialog addStockImport = new AddStockImportDialog((Frame) frame, editedStockImport);
        addStockImport.setLocationRelativeTo(this);
        addStockImport.setVisible(true);
        addStockImport.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });  
    }
}

//    private void openDialog() {
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
//        AddStockImport addStockImport = new AddStockImport((Frame) frame, editedStockImport);
//        addStockImport.setVisible(true);
//
//        addStockImport.addWindowListener(new WindowAdapter() {
//            @Override
//            public void windowClosed(WindowEvent e) {
//                refreshHisTable(st.getId());
//            }
//        });
//    }


//    private void setFromtoObject() {
//    }


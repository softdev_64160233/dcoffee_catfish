/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui;

import com.Dcoffee.catfish.model.Material;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.model.StockImportDetail;
import com.Dcoffee.catfish.service.MaterialService;
import com.Dcoffee.catfish.service.StockImportService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import static java.time.zone.ZoneRulesProvider.refresh;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author User
 */
public class AddStockImport extends javax.swing.JDialog {
    //private final JScrollPane scrMain;

    private MaterialService matService;
    private List<Material> matList;
    StockImportService hisInvoiceService = new StockImportService();
    private StockImport hisInvoice;
    private ArrayList<StockImportDetail> hisInvoiceDetails = new ArrayList<StockImportDetail>();

    //private final StockImportService stockImportService;
    private StockImport editedStockImport;
    
    /**
     * Creates new form AddStockImport
     */
    public AddStockImport(java.awt.Frame parent, StockImport editedStockImport) {
        super(parent, true);
        initComponents();
        //this.scrMain = scrMain;
        ImageIcon icon = new ImageIcon("./leftarrow.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
//        btnBack.setIcon(icon);

        hisInvoice = new StockImport();
        hisInvoice.setUser(UserService.getCurrentUser());
        initStockImportTable();
        tblStockImport.setRowHeight(30);
        tblStockImport.setModel(new AbstractTableModel() {
            String[] hearderHis = {"Name", "Company", "Price", "QTY", "Total"};

            @Override
            public int getRowCount() {
                return hisInvoice.getStockImportDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public String getColumnName(int column) {
                return hearderHis[column];
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
                StockImportDetail hisInvoiceDetail = hisInvoiceDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisInvoiceDetail.getMaterialName();
                    case 1:
                        return hisInvoiceDetail.getStockImportDetailCompany();
                    case 2:
                        return hisInvoiceDetail.getMaterialPrice();
                    case 3:
                        return hisInvoiceDetail.getMaterialQty();
                    case 4:
                        return hisInvoiceDetail.getMaterialTotalPrice();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
                StockImportDetail hisInvoiceDetail = hisInvoiceDetails.get(rowIndex);
                if (columnIndex == 4) {
                    int amount = Integer.parseInt((String) aValue);
                    if (amount < 1) {
                        return;
                    }
                    hisInvoiceDetail.setMaterialQty(amount);
                    hisInvoice.calculateTotal();
                    refresHis();
                }
                if (columnIndex == 2) {
                    String company = (String) aValue;
                    hisInvoiceDetail.setStockImportDetailCompany(company);
                    refresHis();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    case 4:
                        return true;
                    default:
                        return false;
                }
            }

            private void refresHis() {
                tblStockImport.revalidate();
                tblStockImport.repaint();
            }
        }
        );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblStockImport = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblStock);

        tblStockImport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblStockImport);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear)
                    .addComponent(btnDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedDel = tblStockImport.getSelectedRow();
        ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
        StockImportDetail hisInvoiceDetail = hisInvoiceDetails.get(selectedDel);
        if (selectedDel >= 0) {
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete " + hisInvoiceDetail.getMaterialName() + "?", "Caution!!", JOptionPane.YES_NO_OPTION,
                    JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                hisInvoice.delStockImportDetail(hisInvoiceDetail);
            }
            refreshStock();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        ArrayList<StockImportDetail> hisInvoiceDetails = hisInvoice.getStockImportDetail();
        int input = JOptionPane.showConfirmDialog(this,
                "Do you want to clear ?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (input == 0) {
            hisInvoiceDetails.clear();

        }
        refreshStock();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        System.out.println(" " + hisInvoice);
        hisInvoiceService.add(hisInvoice);
        //scrMain.setViewportView(new HistoryInvoicePanel(scrMain));
        ImageIcon icon = new ImageIcon("./checked.png");
        Image image = icon.getImage(); // 
        Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
        icon = new ImageIcon(newimg); // 
        JOptionPane.showMessageDialog(tblStock, "Add material invoice successfully!", "Material Invoice", HEIGHT, icon);
        this.dispose();
    }//GEN-LAST:event_btnSaveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnSave;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblStock;
    private javax.swing.JTable tblStockImport;
    // End of variables declaration//GEN-END:variables

    private void initStockImportTable() {
        matService = new MaterialService();
        matList = matService.getMaterials();
        tblStock.setRowHeight(30);
        tblStock.setModel(new AbstractTableModel() {
            String[] hearderHis = {"Name", "Unit"};

            @Override
            public String getColumnName(int column) {
                return hearderHis[column];
            }

            @Override
            public int getRowCount() {
                return matList.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material mat = matList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return mat.getMaterialName();
                    case 1:
                        return mat.getMaterialUnit();
                    default:
                        return "Unknown";
                }
            }

        });

        tblStock.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblStock.rowAtPoint(e.getPoint());
                int col = tblStock.columnAtPoint(e.getPoint());
                System.out.println(matList.get(row));
                Material matModel = matList.get(row);
                //HistoryInvoiceDetailModel hi = new HistoryInvoiceDetailModel(matModel.getId(), matModel.getKey(), matModel.getName(),
                //matModel.getUnit(), matModel.getPrice(), 1, (int) matModel.getPrice(), "", -1);
                hisInvoice.addStockImportDetail(matModel, 1);
                tblStockImport.repaint();
                tblStockImport.revalidate();
            }

        });
    }

    private void refreshStock() {
        tblStockImport.revalidate();
        tblStockImport.repaint();
    }
}

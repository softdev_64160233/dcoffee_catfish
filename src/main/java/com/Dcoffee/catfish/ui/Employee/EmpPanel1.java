/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui.Employee;

import com.Dcoffee.catfish.ui.*;
import com.Dcoffee.catfish.model.Employee;
import com.Dcoffee.catfish.model.User;
import com.Dcoffee.catfish.service.EmployeeService;
import com.Dcoffee.catfish.service.UserService;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.util.Collections.list;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author HP
 */
public class EmpPanel1 extends javax.swing.JPanel {

    private final EmployeeService empService;
    private List<Employee> empList;
    private Employee editedEmp;
//    private final UserService userService;
    private List<User> list;
    private User editedUser;
    private EmployeeListPanel employeeListPanel;

    /**
     * Creates new form EmpPanel
     */
    public EmpPanel1() {
        initComponents();
        iconSearch.setIcon(new ImageIcon("img/search.png"));
        btnAdd.setIcon(new ImageIcon("img/Addcycle.png"));
        scrContent.setViewportView(new EmployeeListPanel());
    
//        btnSearch.setIcon(new ImageIcon("img/search1.png"));
//        btnDelete.setIcon(new ImageIcon("img/del.png"));
//        btnEdit.setIcon(new ImageIcon("img/edit.png"));
//        btnAdd.setIcon(new ImageIcon("img/addcus.png"));
//        btnPrint.setIcon(new ImageIcon("img/print.png"));
//        userService = new UserService();
//        list = userService.getOrderByUsers();
//        tblEmp.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
//        tblUser.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
//        UIManager.put("OptionPane.messageFont", new Font("Tahoma", Font.PLAIN, 14));
        empService = new EmployeeService();
        empList = empService.getEmployees();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        tfSearch = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        iconSearch = new javax.swing.JLabel();
        scrContent = new javax.swing.JScrollPane();

        jLabel1.setText("jLabel1");

        jScrollPane1.setViewportView(jTree1);

        setBackground(new java.awt.Color(65, 55, 54));
        setPreferredSize(new java.awt.Dimension(688, 494));

        jLabel3.setFont(new java.awt.Font("AngsanaUPC", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("EMPLOYEE");

        jPanel1.setBackground(new java.awt.Color(63, 54, 53));

        tfSearch.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tfSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfSearchActionPerformed(evt);
            }
        });
        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfSearchKeyReleased(evt);
            }
        });

        btnAdd.setFont(new java.awt.Font("MV Boli", 0, 12)); // NOI18N
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(iconSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 366, Short.MAX_VALUE)
                .addComponent(btnAdd)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(iconSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(9, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(scrContent))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrContent, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedEmp = new Employee();
        openDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void tfSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfSearchActionPerformed

    private void tfSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyReleased
        String searchQuery = tfSearch.getText();
        System.out.print(searchQuery);

        if (searchQuery.isEmpty()) {
            employeeListPanel = new EmployeeListPanel();
            scrContent.setViewportView(employeeListPanel);
        } else {
            employeeListPanel.searchAndUpdateList(searchQuery);
        }

    }//GEN-LAST:event_tfSearchKeyReleased

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        EmpDialog1 empDialog = new EmpDialog1((Frame) frame, editedEmp);
        empDialog.setLocationRelativeTo(this);
        empDialog.setVisible(true);
        empDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JLabel iconSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree1;
    private javax.swing.JScrollPane scrContent;
    private javax.swing.JTextField tfSearch;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        empList = empService.getEmployees();
        scrContent.setViewportView(new EmployeeListPanel());
        scrContent.revalidate();
        scrContent.repaint();
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportUtilitys {
    private String mounth;
    private double total;

    public ReportUtilitys(String mounth, double total) {
        this.mounth = mounth;
        this.total = total;
    }
    public ReportUtilitys() {
        this("",0);
    }

    public String getMounth() {
        return mounth;
    }

    public void setMounth(String mounth) {
        this.mounth = mounth;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportUtilitys{" + "mounth=" + mounth + ", total=" + total + '}';
    }
    public static ReportUtilitys fromRS(ResultSet rs) {
        ReportUtilitys obj = new ReportUtilitys();
        try {
            obj.setMounth(rs.getString("current_month"));
            obj.setTotal(rs.getInt("SUM(utility_expense_total)"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportUtilitys.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}

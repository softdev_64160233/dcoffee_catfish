/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import com.Dcoffee.catfish.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class StockImport {
    private int id;
    private Date createdDate;
    private int userId;
    private int stockImportTotal;
    private User user;
    private ArrayList<StockImport> stockImport = new ArrayList<StockImport>();
    private ArrayList<StockImportDetail> stockImportDetail = new ArrayList<StockImportDetail>();
//    private float total;

    public StockImport(int id, Date createdDate, int userId,int stockImportTotal) {
        this.id = id;
        this.createdDate = createdDate;
        this.userId = userId;
        this.stockImportTotal = stockImportTotal;
    }
    
    public StockImport(Date createdDate, int userId,int stockImportTotal) {
        this.id = -1;
        this.createdDate = createdDate;
        this.userId = userId;
        this.stockImportTotal = stockImportTotal;
    }
    public StockImport(int userId) {
        this.id = -1;
        this.createdDate = null;
        this.userId = userId;
        this.stockImportTotal = 0;
    }

    
    public StockImport() {
        this.id = -1;
        this.createdDate = null;
        this.userId = 0;
        this.stockImportTotal = 0;
    }
    
//    public StockImport() {
//        this.id = 0;
//        this.createdDate = createdDate;
//        this.userId = 0;
//        this.stockImportTotal = 0;
//        this.user = user;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStockImportTotal() {
        return stockImportTotal;
    }

    public void setStockImportTotal(int stockImportTotal) {
        this.stockImportTotal = stockImportTotal;
    }

    public ArrayList<StockImport> getStockImport() {
        return stockImport;
    }

    public void setStockImport(ArrayList<StockImport> stockImport) {
        this.stockImport = stockImport;
    }

//    public ArrayList<StockImport> getBillDetail() {
//        return stockImport;
//    }

   
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }
    
//    public void addHisMatDetail(StockImport stockImportDetail) {
//        stockImportDetails.add(stockImportDetail);
//    }
//
//    public void addHisMatDetail(StockImport material) {
//        StockImport md = new StockImport(material.getId(), material.get(), material.getName(), material.getUnit(),
//                material.getRemain(), -1);
//        stockImportDetails.add(md);
//    }
//
//    public void delHisMatDetail(StockImport stockImportDetail) {
//        stockImportDetails.remove(stockImportDetail);
//    }
    
//    public void add(Material material, int i) {
//        StockImportDetail std = new StockImportDetail(userId, material.getMaterialName(), material.getMaterialUnit(), "", userId, userId, userId, stockImportTotal);
//        //BillDetail bd = new BillDetail(material.getMtrID(), material.getMtrName(), material.getMtrPrice());
//        stockImport.add(std);
//        calculateTotal();
//    }
    
//    public ArrayList<StockImport> getStockImportDetail() {
//        return stockImport;
//    }
    
    public ArrayList<StockImportDetail> getStockImportDetail() {
        return stockImportDetail;
    }

    @Override
    public String toString() {
        return "StockImport{" + "id=" + id + ", createdDate=" + createdDate + ", userId=" + userId + ", stockImportTotal=" + stockImportTotal + ", user=" + user + ", stockImport=" + stockImport + ", stockImportDetail=" + stockImportDetail + '}';
    }


    public static StockImport fromRS(ResultSet rs) {
        StockImport stockImport = new StockImport();
        try {
            stockImport.setId(rs.getInt("id"));
            stockImport.setCreatedDate(rs.getDate("stock_import_date"));
            stockImport.setUserId(rs.getInt("user_id"));
            stockImport.setStockImportTotal(rs.getInt("stock_import_total"));

//            Population
            UserDao userDao = new UserDao();
            User user = userDao.get(stockImport.getUserId());
            stockImport.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(StockImport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockImport;
    }
    
    public void delStockImportDetail(StockImportDetail hisInvoice) {
        stockImportDetail.remove(hisInvoice);
        calculateTotal();
    }

//    private void calculateTotal() {
//        int totalQty = 0;
//        float total = 0.0f;
//        for(StockImportDetail std: stockImport) {
//            total += std.getBillDetailTotailprice();
//            totalQty += std.getBillDetailQty();
//        }
//        //this.totalQty = totalQty;
//        this.total = (int) total;
//    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (StockImportDetail hd : stockImportDetail) {
            total += hd.getMaterialTotalPrice();
            totalQty += hd.getMaterialQty();
            //totalQty += hd.getMaterialQty();
        }
        //this.totalQty = totalQty;
        this.stockImportTotal = (int) total;
    }
    
    public void addStockImportDetail(Material material, int qty) {
        StockImportDetail sidetail = new StockImportDetail(-1, material.getMaterialID(), material.getMaterialName(), material.getMaterialUnit(), "", qty, (int) material.getMaterialPrice(), qty * (int) material.getMaterialPrice(), 0);
        //StockImportDetail sidetail = new StockImportDetail(material.getMaterialID(), material.getMaterialName(), material.getMaterialUnit(), material.getMaterialPrice(), qty, qty * (int) material.getMaterialPrice(), "", -1);
        //StockImportDetail sidetail = new StockImportDetail(material.getMaterialID(), material.getMaterialName(), material.getMaterialUnit(), material.getMaterialPrice(), qty, qty * (int) material.getMaterialPrice(), "", -1);
        stockImportDetail.add(sidetail);
        calculateTotal();
    }

//    public ArrayList<StockImportDetail> getStockImportDetail() {
//        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
//    }
}

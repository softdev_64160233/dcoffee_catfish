/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportQty {
    private String orderDate;
    private String productName;
    private String productSize;
    private String productType;
    private int productLevel;
    private int amount;

    public ReportQty(String orderDate, String productName, String productSize, String productType, int productLevel, int amount) {
        this.orderDate = orderDate;
        this.productName = productName;
        this.productSize = productSize;
        this.productType = productType;
        this.productLevel = productLevel;
        this.amount = amount;
    }
    public ReportQty() {
        this("","","","",0,0);
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

   

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getProductLevel() {
        return productLevel;
    }

    public void setProductLevel(int productLevel) {
        this.productLevel = productLevel;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "ReportQty{" + "orderDate=" + orderDate + ", productName=" + productName + ", productSize=" + productSize + ", productType=" + productType + ", productLevel=" + productLevel + ", amount=" + amount + '}';
    }
    public static ReportQty fromRS(ResultSet rs) {
        ReportQty obj = new ReportQty();
        try {
            obj.setOrderDate(rs.getString("order_datetime"));
            obj.setProductName(rs.getString("product_name"));
            obj.setProductSize(rs.getString("product_size"));
            obj.setProductType(rs.getString("product_type"));
            obj.setProductLevel(rs.getInt("product_level"));
            obj.setAmount(rs.getInt("amount"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportQty.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportPayroll {
    private String mount;
    private double totalPayment;

    public ReportPayroll(String mount, double totalPayment) {
        this.mount = mount;
        this.totalPayment = totalPayment;
    }
    public ReportPayroll() {
        this("",0);

    }

    public String getMount() {
        return mount;
    }

    public void setMount(String mount) {
        this.mount = mount;
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(double totalPayment) {
        this.totalPayment = totalPayment;
    }

    @Override
    public String toString() {
        return "ReportPayroll{" + "mount=" + mount + ", totalPayment=" + totalPayment + '}';
    }
    public static ReportPayroll fromRS(ResultSet rs) {
        ReportPayroll obj = new ReportPayroll();
        try {
            obj.setMount(rs.getString("month"));
            obj.setTotalPayment(rs.getDouble("total_payment"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportPayroll.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}

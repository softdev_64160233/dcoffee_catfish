/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportEmployee {
    private int id;
    private String timein;
    private String timeout;
    private int timehour;
    private String date ;
    private double income;

    public ReportEmployee(String timein, String timeout, int timehour,String date, double income) {
        this.timein = timein;
        this.timeout = timeout;
        this.timehour = timehour;
        this.date = date;
        this.income = income;
    }
    public ReportEmployee() {
        this("","",0,"",0);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public int getTimehour() {
        return timehour;
    }

    public void setTimehour(int timehour) {
        this.timehour = timehour;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ReportEmployee{" + "id=" + id + ", timein=" + timein + ", timeout=" + timeout + ", timehour=" + timehour + ", date=" + date + ", income=" + income + '}';
    }

   

    
    public static ReportEmployee fromRS(ResultSet rs) {
        ReportEmployee obj = new ReportEmployee();
        try {
            
            obj.setTimein(rs.getString("check_time_in"));
            obj.setTimeout(rs.getString("check_time_out"));
            obj.setTimehour(rs.getInt("check_time_hour"));
            obj.setDate(rs.getString("payroll_date"));
            obj.setIncome(rs.getInt("income"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}

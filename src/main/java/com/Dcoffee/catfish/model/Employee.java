/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import com.Dcoffee.catfish.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class Employee {

    

    private int id;
    private String name;
    private String surname;
    private String phone;
    private String idNumber;
    private String address;
    private String email;
    private double hourlyRate;
    private int userId;
    private User user;
    private int selectedIndex;
    private Iterable<Employee> empList;

    public Employee(int id, String name, String surname, String phone, String idNumber, String address, String email, double hourlyRate, int userId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.idNumber = idNumber;
        this.address = address;
        this.email = email;
        this.hourlyRate = hourlyRate;
        this.userId = userId;
    }

    public Employee(String name, String surname, String phone, String idNumber, String address, String email, double hourlyRate, int userId) {
        this.id = -1;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.idNumber = idNumber;
        this.address = address;
        this.email = email;
        this.hourlyRate = hourlyRate;
        this.userId = userId;

    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.surname = "";
        this.phone = "";
        this.idNumber = "";
        this.address = "";
        this.email = "";
        this.hourlyRate = 0;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }


    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", idNumber=" + idNumber + ", address=" + address + ", email=" + email + ", hourlyRate=" + hourlyRate + ", userId=" + userId + ", user=" + user + '}';
    }

    

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setName(rs.getString("employee_name"));
            employee.setSurname(rs.getString("employee_surename"));
            employee.setPhone(rs.getString("employee_phone"));
            employee.setAddress(rs.getString("employee_address"));
            employee.setEmail(rs.getString("employee_email"));
            employee.setIdNumber(rs.getString("employee_idNumber"));
            employee.setHourlyRate(rs.getDouble("employee_hourly_rate"));
            employee.setUserId(rs.getInt("user_id"));
            //populate
            UserDao userDao = new UserDao();
            User user = userDao.get(employee.getUserId());
            employee.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
    public boolean isValid(){
        return this.name.length()>0 &&
               this.surname.length()>0 &&
               this.phone.length()==10 &&
               this.address.length()>0 &&
                this.idNumber.length()>=13;
    }

    public int getId(int selectedIndex) {
 return selectedIndex;
          }
    public Employee getEmployeeById(int selectedIndex) {
    for (Employee employee : empList) {
        if (employee.getId() == selectedIndex) {
            return employee;
        }
    }
    // หากไม่พบ Employee ที่มี Id เท่ากับ selectedIndex
    throw new IllegalArgumentException("Employee with Id not found: " + selectedIndex);
}
    }


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

/**
 *
 * @author Miso
 */
public class UtilityExpense {

    private int utilityExpenseId;
    private int utilityExpenseType;
    private Date utilityExpensePeriod;
    private double utilityExpenseUnit;
    private double utilityExpenseTotal;

    public UtilityExpense(int utilityExpenseId, int utilityExpenseType, Date utilityExpensePeriod, double utilityExpenseUnit, double utilityExpenseTotal) {
        this.utilityExpenseId = utilityExpenseId;
        this.utilityExpenseType = utilityExpenseType;
        this.utilityExpensePeriod = utilityExpensePeriod;
        this.utilityExpenseUnit = utilityExpenseUnit;
        this.utilityExpenseTotal = utilityExpenseTotal;
    }

    public UtilityExpense(int utilityExpenseType, Date utilityExpensePeriod, double utilityExpenseUnit, double utilityExpenseTotal) {
        this.utilityExpenseId = -1;
        this.utilityExpenseType = utilityExpenseType;
        this.utilityExpensePeriod = utilityExpensePeriod;
        this.utilityExpenseUnit = utilityExpenseUnit;
        this.utilityExpenseTotal = utilityExpenseTotal;
    }

    public UtilityExpense() {
        this.utilityExpenseId = -1;
        this.utilityExpenseType = 1;
        this.utilityExpensePeriod = null;
        this.utilityExpenseUnit = 0;
        this.utilityExpenseTotal = 0;
    }

    public int getUtilityExpenseId() {
        return utilityExpenseId;
    }

    public Date getUtilityExpensePeriod() {
        return utilityExpensePeriod;
    }

    public void setUtilityExpensePeriod(Date utilityExpensePeriod) {
        this.utilityExpensePeriod = utilityExpensePeriod;
    }

    public void setUtilityExpenseId(int utilityExpenseId) {
        this.utilityExpenseId = utilityExpenseId;
    }

    public int getUtilityExpenseType() {
        return utilityExpenseType;
    }

    public void setUtilityExpenseType(int utilityExpenseType) {
        this.utilityExpenseType = utilityExpenseType;
    }

    public double getUtilityExpenseUnit() {
        return utilityExpenseUnit;
    }

    public void setUtilityExpenseUnit(double utilityExpenseUnit) {
        this.utilityExpenseUnit = utilityExpenseUnit;
    }

    public double getUtilityExpenseTotal() {
        return utilityExpenseTotal;
    }

    public void setUtilityExpenseTotal(double utilityExpenseTotal) {
        this.utilityExpenseTotal = utilityExpenseTotal;
    }

    @Override
    public String toString() {
        return "UtilityExpense{" + "utilityExpenseId=" + utilityExpenseId + ", utilityExpenseType=" + utilityExpenseType + ", utilityExpensePeriod=" + utilityExpensePeriod + ", utilityExpenseUnit=" + utilityExpenseUnit + ", utilityExpenseTotal=" + utilityExpenseTotal + '}';
    }



  

    

    public static UtilityExpense fromRS(ResultSet rs) {
        UtilityExpense utilityexpense = new UtilityExpense();
        try {
            utilityexpense.setUtilityExpenseId(rs.getInt("utility_expense_id"));
            utilityexpense.setUtilityExpenseType(rs.getInt("utility_expense_type"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String period = rs.getString("utility_expense_period");
            try {
                utilityexpense.setUtilityExpensePeriod(df.parse(period));
            } catch (ParseException ex) {
                Logger.getLogger(UtilityExpense.class.getName()).log(Level.SEVERE, null, ex);
            }
            utilityexpense.setUtilityExpenseUnit(rs.getInt("utility_expense_unit"));
            utilityexpense.setUtilityExpenseTotal(rs.getDouble("utility_expense_total"));
        } catch (SQLException ex) {
            Logger.getLogger(UtilityExpense.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return utilityexpense;
    }
    public boolean isValid(){
        return this.utilityExpenseUnit >=0 &&
                 this.utilityExpenseTotal >0.0;
    }
}

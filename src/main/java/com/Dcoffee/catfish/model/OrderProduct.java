/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class OrderProduct {

    private int orderProductId;
    private int orderId;
    private String productName;
    private String productSize;
    private String productType;
    private String productSweetness;
    private int productAmount;
    private double productPrice;
    private double total;

    public OrderProduct(int orderProductId, int orderId, String productName, String productSize, String productType, String productSweetness, int productAmount, double productPrice) {
        this.orderProductId = orderProductId;
        this.orderId = orderId;
        this.productName = productName;
        this.productSize = productSize;
        this.productType = productType;
        this.productSweetness = productSweetness;
        this.productAmount = productAmount;
        this.productPrice = productPrice;
        this.total = productPrice * productAmount;
    }

    public OrderProduct(int orderId, String productName, String productSize, String productType, String productSweetness, int productAmount, double productPrice) {
        this.orderProductId = -1;
        this.orderId = orderId;
        this.productName = productName;
        this.productSize = productSize;
        this.productType = productType;
        this.productSweetness = productSweetness;
        this.productAmount = productAmount;
        this.productPrice = productPrice;
        this.total = productPrice * productAmount;
    }

    public OrderProduct() {
        this.orderProductId = -1;
        this.orderId = -1;
        this.productName = "";
        this.productSize = "N";
        this.productType = "";
        this.productSweetness = "";
        this.productAmount = 0;
        this.productPrice = 0;
        this.total = productPrice * productAmount;
    }

    public int getOrderProductId() {
        return orderProductId;
    }

    public void setOrderProductId(int orderProductId) {
        this.orderProductId = orderProductId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductSweetness() {
        return productSweetness;
    }

    public void setProductSweetness(String productSweetness) {
        this.productSweetness = productSweetness;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "OrderProduct{" + "orderProductId=" + orderProductId + ", orderId=" + orderId + ", productName=" + productName + ", productSize=" + productSize + ", productType=" + productType + ", productSweetness=" + productSweetness + ", productAmount=" + productAmount + ", productPrice=" + productPrice + ", total=" + total + '}';
    }

    public static OrderProduct fromRS(ResultSet rs) {
        //System.out.println("enter fromRS here");
        OrderProduct orderProduct = new OrderProduct();
        try {
            orderProduct.setOrderProductId(rs.getInt("order_product_id"));
            orderProduct.setOrderId(rs.getInt("order_id"));
            orderProduct.setProductName(rs.getString("product_name"));
            orderProduct.setProductSize(rs.getString("product_size"));
            orderProduct.setProductType(rs.getString("product_type"));
            orderProduct.setProductSweetness(rs.getString("product_level"));
            orderProduct.setProductAmount(rs.getInt("amount"));
            orderProduct.setProductPrice(rs.getDouble("order_product_price"));
            orderProduct.setTotal(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return orderProduct;
    }

    void add(ArrayList<OrderProduct> orderProducts) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}

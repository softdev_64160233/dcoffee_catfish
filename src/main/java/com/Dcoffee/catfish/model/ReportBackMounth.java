/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportBackMounth {
    private String mounth;
    private double totalSum;
    private double discountSum;

    public ReportBackMounth(String mounth, double totalSum, double discountSum) {
        this.mounth = mounth;
        this.totalSum = totalSum;
        this.discountSum = discountSum;
    }
    public ReportBackMounth() {
        this("",0,0);

    }

    public String getMounth() {
        return mounth;
    }

    public void setMounth(String mounth) {
        this.mounth = mounth;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public double getDiscountSum() {
        return discountSum;
    }

    public void setDiscountSum(double discountSum) {
        this.discountSum = discountSum;
    }

    @Override
    public String toString() {
        return "ReportBackMounth{" + "mounth=" + mounth + ", totalSum=" + totalSum + ", discountSum=" + discountSum + '}';
    }
    public static ReportBackMounth fromRS(ResultSet rs) {
        ReportBackMounth obj = new ReportBackMounth();
        try {
            obj.setMounth(rs.getString("month"));
            obj.setTotalSum(rs.getInt("total_sum"));
            obj.setDiscountSum(rs.getInt("discount_sum"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportBackMounth.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class Order {

    private int orderId;
    private int cusId;
    private int userId;
    private double orderTotal; //totalQty
    private double orderDiscount;
    private double orderCash;
    private double orderChange;
    private String orderDateTime;

    public Order(int orderId,int cusId, int userId, double orderTotal, double orderDiscount, double orderCash, double orderChange, String orderDateTime) {
        this.orderId = orderId;
        this.cusId = cusId;
        this.userId = userId;
        this.orderTotal = orderTotal;
        this.orderDiscount = orderDiscount;
        this.orderCash = orderCash;
        this.orderChange = orderChange;
        this.orderDateTime = orderDateTime;
    }

    public Order(int cusId, int userId, double orderTotal, double orderDiscount, double orderCash, double orderChange, String orderDateTime) {
        this.orderId = -1;
        this.cusId = cusId;
        this.userId = userId;
        this.orderTotal = orderTotal;
        this.orderDiscount = orderDiscount;
        this.orderCash = orderCash;
        this.orderChange = orderChange;
        this.orderDateTime = orderDateTime;
    }

    public Order() {
        this.orderId = -1;
        this.cusId = 0;
        this.userId = 0;
        this.orderTotal = 0;
        this.orderDiscount = 0;
        this.orderCash = 0;
        this.orderChange = 0;
        this.orderDateTime = "";
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }


    public int getCusId() {
        return cusId;
    }


    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }



    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public double getOrderCash() {
        return orderCash;
    }

    public void setOrderCash(double orderCash) {
        this.orderCash = orderCash;
    }

    public double getOrderChange() {
        return orderChange;
    }

    public void setOrderChange(double orderChange) {
        this.orderChange = orderChange;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    @Override
    public String toString() {
        return "Order{" + "orderId=" + orderId + ", cusId=" + cusId + ", userId=" + userId + ", orderTotal=" + orderTotal + ", orderDiscount=" + orderDiscount + ", orderCash=" + orderCash + ", orderChange=" + orderChange + ", orderDateTime=" + orderDateTime + '}';
    }
    
    public static Order fromRS(ResultSet rs) {
        Order order = new Order();
        try {
            order.setOrderId(rs.getInt("order_id"));
            order.setCusId(rs.getInt("cus_id"));
            order.setUserId(rs.getInt("user_id"));
            order.setOrderTotal(rs.getDouble("total"));
            order.setOrderDiscount(rs.getDouble("discount"));
            order.setOrderCash(rs.getDouble("cash"));
            order.setOrderChange(rs.getDouble("change"));
            order.setOrderDateTime(rs.getString("order_datetime"));
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return order;
    }
}

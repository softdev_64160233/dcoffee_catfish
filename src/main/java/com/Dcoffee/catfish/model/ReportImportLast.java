/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportImportLast {
    private double totalImportForLastMonth;

    public ReportImportLast(double totalImportForLastMonth) {
        this.totalImportForLastMonth = totalImportForLastMonth;
    }
    public ReportImportLast() {
        this(0);
    }

    public double getTotalImportForLastMonth() {
        return totalImportForLastMonth;
    }

    public void setTotalImportForLastMonth(double totalImportForLastMonth) {
        this.totalImportForLastMonth = totalImportForLastMonth;
    }

    @Override
    public String toString() {
        return "ReportImportLast{" + "totalImportForLastMonth=" + totalImportForLastMonth + '}';
    }
    public static ReportImportLast fromRS(ResultSet rs) {
        ReportImportLast obj = new ReportImportLast();
        try {
            
            obj.setTotalImportForLastMonth(rs.getDouble("total_import_for_current_month"));


        } catch (SQLException ex) {
            Logger.getLogger(ReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
}

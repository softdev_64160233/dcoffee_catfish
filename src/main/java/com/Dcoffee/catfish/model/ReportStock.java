/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportStock {
    private String materialName;
    private int materialQty;
    private int materialMin;

    public ReportStock(String materialName, int materialQty, int materialMin) {
        this.materialName = materialName;
        this.materialQty = materialQty;
        this.materialMin = materialMin;
    }
    public ReportStock() {
        this("",0,0);
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public int getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(int materialQty) {
        this.materialQty = materialQty;
    }

    public int getMaterialMin() {
        return materialMin;
    }

    public void setMaterialMin(int materialMin) {
        this.materialMin = materialMin;
    }

    @Override
    public String toString() {
        return "ReportStock{" + "materialName=" + materialName + ", materialQty=" + materialQty + ", materialMin=" + materialMin + '}';
    }
    
    public static ReportStock fromRS(ResultSet rs) {
        ReportStock obj = new ReportStock();
        try {
            
            obj.setMaterialName(rs.getString("material_name"));
            obj.setMaterialQty(rs.getInt("material_qty"));
            obj.setMaterialMin(rs.getInt("material_min"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}

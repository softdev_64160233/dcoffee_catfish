/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportUtilitysPage {
    private String mounth;
    private double total;

    public ReportUtilitysPage(String mounth, double total) {
        this.mounth = mounth;
        this.total = total;
    }
    public ReportUtilitysPage() {
        this("",0);
    }

    public String getMounth() {
        return mounth;
    }

    public void setMounth(String mounth) {
        this.mounth = mounth;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportUtilitys{" + "mounth=" + mounth + ", total=" + total + '}';
    }
    public static ReportUtilitysPage fromRS(ResultSet rs) {
        ReportUtilitysPage obj = new ReportUtilitysPage();
        try {
            obj.setMounth(rs.getString("Month"));
            obj.setTotal(rs.getInt("total_sum"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportUtilitysPage.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}

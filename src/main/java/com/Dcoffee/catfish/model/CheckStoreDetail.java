/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckStoreDetail {
    private int id;
    private int checkstoreId;
    private int materialId;
    private String  materialName;
    private int materialQty;

    public CheckStoreDetail(int id, int checkstoreId, int materialId, String materialName, int materialQty) {
        this.id = id;
        this.checkstoreId = checkstoreId;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialQty = materialQty;
    }

    public CheckStoreDetail( int checkstoreId, int materialId, String materialName, int materialQty) {
        this.id = -1;
        this.checkstoreId = checkstoreId;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialQty = materialQty;
    }
    
    public CheckStoreDetail() {
        this.id = -1;
        this.checkstoreId = 0;
        this.materialId = 0;
        this.materialName = "";
        this.materialQty = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCheckstoreId() {
        return checkstoreId;
    }

    public void setCheckstoreId(int checkstoreId) {
        this.checkstoreId = checkstoreId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public int getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(int materialQty) {
        this.materialQty = materialQty;
    }

    @Override
    public String toString() {
        return "CheckStoreDetail{" + "id=" + id + ", checkstoreId=" + checkstoreId + ", materialId=" + materialId + ", materialName=" + materialName + ", materialQty=" + materialQty + '}';
    }

    
    public static CheckStoreDetail fromRS(ResultSet rs) {
        CheckStoreDetail checkStoreDetail = new CheckStoreDetail();
        try {
            checkStoreDetail.setId(rs.getInt("check_detail_id"));
            checkStoreDetail.setCheckstoreId(rs.getInt("check_store_id"));
            checkStoreDetail.setMaterialId(rs.getInt("material_id"));
            checkStoreDetail.setMaterialName(rs.getString("material_name"));
            checkStoreDetail.setMaterialQty(rs.getInt("material_qty"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStoreDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStoreDetail;
    }
}

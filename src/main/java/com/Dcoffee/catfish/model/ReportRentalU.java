/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author STDG_077
 */
public class ReportRentalU {
    private String mounth;
    private double total;

    public ReportRentalU(String mounth, double total) {
        this.mounth = mounth;
        this.total = total;
    }
    
    public ReportRentalU() {
        this("",0);
    }

    public String getMounth() {
        return mounth;
    }

    public void setMounth(String mounth) {
        this.mounth = mounth;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportRentalU{" + "mounth=" + mounth + ", total=" + total + '}';
    }
    public static ReportRentalU fromRS(ResultSet rs) {
        ReportRentalU obj = new ReportRentalU();
        try {
            
            obj.setMounth(rs.getString("Month"));
            obj.setTotal(rs.getInt("total"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportRentalU.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
}

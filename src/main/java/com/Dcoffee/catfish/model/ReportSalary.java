/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportSalary {
    private String name;
    private String surname;
    private double ratdHour;
    private String dateIn;
    private String dateout;
    private int hour;
    private String payDate;
    private double total;

    public ReportSalary(String name, String surname, double ratdHour, String dateIn, String dateout, int hour, String payDate, double total) {
        this.name = name;
        this.surname = surname;
        this.ratdHour = ratdHour;
        this.dateIn = dateIn;
        this.dateout = dateout;
        this.hour = hour;
        this.payDate = payDate;
        this.total = total;
    }
    public ReportSalary() {
        this("","",0,"","",0,"",0);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public double getRatdHour() {
        return ratdHour;
    }

    public void setRatdHour(double ratdHour) {
        this.ratdHour = ratdHour;
    }

    public String getDateIn() {
        return dateIn;
    }

    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    public String getDateout() {
        return dateout;
    }

    public void setDateout(String dateout) {
        this.dateout = dateout;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ReportSalary{" + "name=" + name + ", surname=" + surname + ", ratdHour=" + ratdHour + ", dateIn=" + dateIn + ", dateout=" + dateout + ", hour=" + hour + ", payDate=" + payDate + ", total=" + total + '}';
    }
     public static ReportSalary fromRS(ResultSet rs) {
        ReportSalary obj = new ReportSalary();
        try {
            obj.setName(rs.getString("employee_name"));
            obj.setSurname(rs.getString("employee_surename"));
            obj.setRatdHour(rs.getDouble("employee_hourly_rate"));
            obj.setDateIn(rs.getString("check_time_in"));
            obj.setDateout(rs.getString("check_time_out"));
            obj.setHour(rs.getInt("check_time_hour"));
            obj.setPayDate(rs.getString("payroll_date"));
            obj.setTotal(rs.getDouble("total"));
            return obj;
        } catch (SQLException ex) {
            Logger.getLogger(ReportSalary.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class StockImportDetail {
    private int id;
    private int materialId;
    private String materialName;
    private String materialUnit;
    private String stockImportDetailCompany;
    private int materialQty;
    private int materialPrice;
    private int materialTotalPrice;
    private int stockImportId;

    public StockImportDetail(int id, int materialId, String materialName, String materialUnit, String stockImportDetailCompany, int materialQty, int materialPrice, int materialTotalPrice, int stockImportId) {
        this.id = id;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.stockImportDetailCompany = stockImportDetailCompany;
        this.materialQty = materialQty;
        this.materialPrice = materialPrice;
        this.materialTotalPrice = materialTotalPrice;
        this.stockImportId = stockImportId;
    }

        public StockImportDetail(int materialId, String materialName, String materialUnit, String stockImportDetailCompany, int materialQty, int materialPrice, int materialTotalPrice, int stockImportId) {
        this.id = -1;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.stockImportDetailCompany = stockImportDetailCompany;
        this.materialQty = materialQty;
        this.materialPrice = materialPrice;
        this.materialTotalPrice = materialTotalPrice;
        this.stockImportId = stockImportId;
    }
    
    public StockImportDetail() {
        this.id = -1;
        this.materialId = 0;
        this.materialName = "";
        this.materialUnit = "";
        this.stockImportDetailCompany = "";
        this.materialQty = 0;
        this.materialQty = 0;
        this.materialPrice = 0;
        this.stockImportId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialUnit() {
        return materialUnit;
    }

    public void setMaterialUnit(String materialUnit) {
        this.materialUnit = materialUnit;
    }

    public String getStockImportDetailCompany() {
        return stockImportDetailCompany;
    }

    public void setStockImportDetailCompany(String stockImportDetailCompany) {
        this.stockImportDetailCompany = stockImportDetailCompany;
    }

    public int getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(int materialQty) {
        this.materialQty = materialQty;
        //materialTotalPrice = (int) (materialQty * materialPrice);
    }

    public int getStockImportId() {
        return stockImportId;
    }

    public void setStockImportId(int stockImportId) {
        this.stockImportId = stockImportId;
    }

    public int getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(int materialPrice) {
        this.materialPrice = materialPrice;
        //materialTotalPrice = (int) (materialPrice * materialPrice);
    }

    public int getMaterialTotalPrice() {
        return materialTotalPrice;
    }

    public void setMaterialTotalPrice(int materialTotalPrice) {
        this.materialTotalPrice = materialTotalPrice;
    }

    @Override
    public String toString() {
        return "StockImportDetail{" + "id=" + id + ", materialId=" + materialId + ", materialName=" + materialName + ", materialUnit=" + materialUnit + ", stockImportDetailCompany=" + stockImportDetailCompany + ", materialQty=" + materialQty + ", materialPrice=" + materialPrice + ", materialTotalPrice=" + materialTotalPrice + ", stockImportId=" + stockImportId + '}';
    }
    
    

    public static StockImportDetail fromRS(ResultSet rs) {
        StockImportDetail stockImportDetail = new StockImportDetail();
        try {
            stockImportDetail.setId(rs.getInt("id"));
            stockImportDetail.setMaterialId(rs.getInt("material_id"));
            stockImportDetail.setMaterialQty(rs.getInt("material_qty"));
            stockImportDetail.setMaterialPrice(rs.getInt("material_price"));
            stockImportDetail.setMaterialTotalPrice(rs.getInt("material_totalPrice"));
            stockImportDetail.setMaterialName(rs.getString("material_name"));
            stockImportDetail.setMaterialUnit(rs.getString("material_unit"));
            stockImportDetail.setStockImportDetailCompany(rs.getString("stock_import_detail_company"));
            stockImportDetail.setStockImportId(rs.getInt("stock_import_id"));
        } catch (SQLException ex) {
            Logger.getLogger(StockImportDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return stockImportDetail;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportProductBack {
    private String productName;
    private int totalSold;
    private int price;

    public ReportProductBack(String productName, int totalSold, int price) {
        this.productName = productName;
        this.totalSold = totalSold;
        this.price = price;
    }
    public ReportProductBack() {
        this("",0,0);
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(int totalSold) {
        this.totalSold = totalSold;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ReportProductBack{" + "productName=" + productName + ", totalSold=" + totalSold + ", price=" + price + '}';
    }
    public static ReportProductBack fromRS(ResultSet rs) {
        ReportProductBack obj = new ReportProductBack();
        try {
            
            obj.setProductName(rs.getString("product_name"));
            obj.setTotalSold(rs.getInt("total_sold"));
            obj.setPrice(rs.getInt("total_price"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportProductBack.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
    
}

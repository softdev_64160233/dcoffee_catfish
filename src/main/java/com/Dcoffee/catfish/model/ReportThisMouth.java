/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportThisMouth {
    private int totalCheckTime;

    public ReportThisMouth(int totalCheckTime) {
        this.totalCheckTime = totalCheckTime;
    }
    public ReportThisMouth() {
        this(0);
    }

    public int getTotalCheckTime() {
        return totalCheckTime;
    }

    public void setTotalCheckTime(int totalCheckTime) {
        this.totalCheckTime = totalCheckTime;
    }

    @Override
    public String toString() {
        return "ReportThisMouth{" + "totalCheckTime=" + totalCheckTime + '}';
    }
    public static ReportThisMouth fromRS(ResultSet rs) {
        ReportThisMouth obj = new ReportThisMouth();
        try {
            
            obj.setTotalCheckTime(rs.getInt("TotalCheckTime"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportThisMouth.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }  
    
}

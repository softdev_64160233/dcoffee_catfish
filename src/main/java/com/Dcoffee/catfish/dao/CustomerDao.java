/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class CustomerDao implements Dao<Customer> {

    @Override
    public Customer get(int id) {
        Customer item = null;
        String sql = "SELECT * FROM customer WHERE cus_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public Customer getCustomersTel(String tel) {
        Customer item = null;
        String sql = "SELECT * FROM customer WHERE cus_tel=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, tel);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Customer> getAll() {

        ArrayList<Customer> list = new ArrayList();
        String sql = "SELECT * FROM customer";
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer item = Customer.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Customer> getAll(String where, String order) {
        ArrayList<Customer> list = new ArrayList<Customer>();
        String sql = "SELECT * FROM customer where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer item = Customer.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Customer> getAll(String order) {

        ArrayList<Customer> list = new ArrayList<Customer>();

        String sql = "SELECT * FROM customer  ORDER BY" + order;
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Customer item = Customer.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        DatabaseHelper.close();
        return list;
    }

    @Override
    public Customer save(Customer obj) {
        String sql = "INSERT INTO customer (cus_name, cus_surname, cus_tel, cus_point)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCusName());
            stmt.setString(2, obj.getCusSurname());
            stmt.setString(3, obj.getCusTel());
            stmt.setInt(4, obj.getCusPoint());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setCusId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Customer update(Customer obj) {
        String sql = "UPDATE customer"
                + " SET cus_name = ?, cus_surname = ?, cus_tel = ?, cus_point = ?"
                + " WHERE cus_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCusName());
            stmt.setString(2, obj.getCusSurname());
            stmt.setString(3, obj.getCusTel());
            stmt.setInt(4, obj.getCusPoint());
            stmt.setDouble(5, obj.getCusId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Customer obj) {
        String sql = "DELETE FROM customer WHERE cus_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCusId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public Customer getLast() {
        Customer item = null;
        String sql = "SELECT * FROM customer ORDER BY cus_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Customer.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

}

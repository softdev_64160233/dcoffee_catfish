/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.Order;
import com.Dcoffee.catfish.model.UtilityExpense;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fewxi
 */
public class UtilityExpenseDao implements Dao<UtilityExpense> {

    @Override
    public UtilityExpense get(int id) {
        UtilityExpense utilityexpense = null;
        String sql = "SELECT * FROM utility_expense WHERE utility_expense_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                utilityexpense = UtilityExpense.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return utilityexpense;
    }

    public List<UtilityExpense> getAll() {
        ArrayList<UtilityExpense> list = new ArrayList();
        String sql = "SELECT * FROM utility_expense";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                UtilityExpense utilityexpense = UtilityExpense.fromRS(rs);
                list.add(utilityexpense);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<UtilityExpense> getAll(String where, String order) {
        ArrayList<UtilityExpense> list = new ArrayList();
        String sql = "SELECT * FROM utility_expense where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                UtilityExpense utilityexpense = UtilityExpense.fromRS(rs);
                list.add(utilityexpense);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<UtilityExpense> getAll(String order) {
        ArrayList<UtilityExpense> list = new ArrayList();
        String sql = "SELECT * FROM utility_expense  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                UtilityExpense utilityexpense = UtilityExpense.fromRS(rs);
                list.add(utilityexpense);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public UtilityExpense save(UtilityExpense obj) {

        String sql = "INSERT INTO utility_expense (utility_expense_type,utility_expense_period,utility_expense_unit,utility_expense_total)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setInt(1, obj.getUtilityExpenseType());
            stmt.setString(2, df.format(obj.getUtilityExpensePeriod()));
            stmt.setDouble(3, obj.getUtilityExpenseUnit());
            stmt.setDouble(4, obj.getUtilityExpenseTotal());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setUtilityExpenseId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public UtilityExpense update(UtilityExpense obj) {
        String sql = "UPDATE utility_expense"
                + " SET utility_expense_type = ?, utility_expense_period = ?, utility_expense_unit = ?, utility_expense_total = ? "
                + " WHERE utility_expense_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setInt(1, obj.getUtilityExpenseType());
            stmt.setString(2, df.format(obj.getUtilityExpensePeriod()));
            stmt.setDouble(3, obj.getUtilityExpenseUnit());
            stmt.setDouble(4, obj.getUtilityExpenseTotal());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(UtilityExpense obj) {
        String sql = "DELETE FROM utility_expense WHERE utility_expense_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUtilityExpenseId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<UtilityExpense> getDate(String dateTime) {
    ArrayList<UtilityExpense> list = new ArrayList<>();
    String sql = "SELECT * FROM utility_expense WHERE DATE(utility_expense_period) = ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, dateTime);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            UtilityExpense item = UtilityExpense.fromRS(rs);
            list.add(item);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}
public List<UtilityExpense> getDateByTime(String begin ,String end) {
        ArrayList<UtilityExpense> list = new ArrayList<>();
        String sql = "SELECT * FROM utility_expense\n"
                + "WHERE DATE(utility_expense_period) >=? \n"
                + "AND DATE(utility_expense_period) <= ?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                UtilityExpense item = UtilityExpense.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

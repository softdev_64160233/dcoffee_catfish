/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ProductDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        Product item = null;
        String sql = "SELECT * FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Product.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

//    public Product getProductsTel(String tel) {
//        Product item = null;
//        String sql = "SELECT * FROM product WHERE cus_tel=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, tel);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                item = Product.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return item;
//    }
    @Override
    public List<Product> getAll() {

        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Product> getAll(String where, String order) {
        ArrayList<Product> list = new ArrayList<Product>();
        String sql = "SELECT * FROM product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Product> getAll(String order) {

        ArrayList<Product> list = new ArrayList<Product>();

        String sql = "SELECT * FROM product  ORDER BY" + order;
        Connection conn = null;
        try {
            conn = DatabaseHelper.getConnect();

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product item = Product.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        DatabaseHelper.close();
        return list;
    }

    @Override
    public Product save(Product obj) {
        String sql = "INSERT INTO product (product_name, product_price, product_size, product_sweet_level, product_type,category_id)"
                + "VALUES(?, ?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setDouble(2, obj.getProductPrice());
            stmt.setString(3, obj.getProductSize());
            stmt.setString(4, obj.getProductSweetLevel());
            stmt.setString(5, obj.getProductType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Product update(Product obj) {
        String sql = "UPDATE product"
                + " SET product_name = ?, product_price = ?, product_size = ?, product_sweet_level = ?, product_type = ?, category_id = ?"
                + " WHERE product_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProductName());
            stmt.setDouble(2, obj.getProductPrice());
            stmt.setString(3, obj.getProductSize());
            stmt.setString(4, obj.getProductSweetLevel());
            stmt.setString(5, obj.getProductType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Product obj) {
        String sql = "DELETE FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<Product> getAllBySearch(String search) {
    ArrayList<Product> list = new ArrayList();
    String sql = "SELECT product_id,\n"
            + "       product_name,\n"
            + "       product_price,\n"
            + "       product_size,\n"
            + "       product_sweet_level,\n"
            + "       product_type,\n"
            + "       category_id\n"
            + "FROM product\n"
            + "WHERE product_name LIKE ?"; // ลบเครื่องหมาย '?' ออก

    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        // เราไม่ต้องเพิ่มคำอักษร '%' ในนี้เนื่องจากเราจะทำการเพิ่มคำอักษร '%' เข้าไปในค่า search ที่รับเข้ามา
        stmt.setString(1, search + "%"); // เพิ่ม '%' ในคำสั่ง SQL
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Product product = Product.fromRS(rs);
            list.add(product);
        }

    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}


}

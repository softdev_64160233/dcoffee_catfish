/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.ReportElectricityU;
import com.Dcoffee.catfish.model.ReportInternetU;
import com.Dcoffee.catfish.model.ReportRentalU;
import com.Dcoffee.catfish.model.ReportUtilitys;
import com.Dcoffee.catfish.model.ReportUtilitysPage;
import com.Dcoffee.catfish.model.ReportWaterU;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author STDG_077
 */
public class ReportUtilityDao implements Dao<ReportUtilitys> {

    @Override
    public ReportUtilitys get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportUtilitys> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportUtilitys save(ReportUtilitys obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportUtilitys update(ReportUtilitys obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ReportUtilitys obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportUtilitys> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ReportUtilitysPage> getUtilityByMounth() {
        ArrayList<ReportUtilitysPage> list = new ArrayList();
        String sql = "SELECT CASE WHEN strftime('%m', utility_expense_period) = '01' THEN 'January' WHEN strftime('%m', utility_expense_period) = '02' THEN 'February' WHEN strftime('%m', utility_expense_period) = '03' THEN 'March' WHEN strftime('%m', utility_expense_period) = '04' THEN 'April' WHEN strftime('%m', utility_expense_period) = '05' THEN 'May' WHEN strftime('%m', utility_expense_period) = '06' THEN 'June' WHEN strftime('%m', utility_expense_period) = '07' THEN 'July' WHEN strftime('%m', utility_expense_period) = '08' THEN 'August' WHEN strftime('%m', utility_expense_period) = '09' THEN 'September' WHEN strftime('%m', utility_expense_period) = '10' THEN 'October' WHEN strftime('%m', utility_expense_period) = '11' THEN 'November' WHEN strftime('%m', utility_expense_period) = '12' THEN 'December' END AS Month,\n"
                + "       SUM(utility_expense_total) AS total_sum\n"
                + "  FROM utility_expense\n"
                + " WHERE utility_expense_type\n"
                + " GROUP BY Month\n"
                + " ORDER BY strftime('%m', utility_expense_period);";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportUtilitysPage obj = ReportUtilitysPage.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportElectricityU> getElectricityByMounth() {
        ArrayList<ReportElectricityU> list = new ArrayList();
        String sql = "SELECT CASE WHEN strftime('%m', utility_expense_period) = '01' THEN 'January' WHEN strftime('%m', utility_expense_period) = '02' THEN 'February' WHEN strftime('%m', utility_expense_period) = '03' THEN 'March' WHEN strftime('%m', utility_expense_period) = '04' THEN 'April' WHEN strftime('%m', utility_expense_period) = '05' THEN 'May' WHEN strftime('%m', utility_expense_period) = '06' THEN 'June' WHEN strftime('%m', utility_expense_period) = '07' THEN 'July' WHEN strftime('%m', utility_expense_period) = '08' THEN 'August' WHEN strftime('%m', utility_expense_period) = '09' THEN 'September' WHEN strftime('%m', utility_expense_period) = '10' THEN 'October' WHEN strftime('%m', utility_expense_period) = '11' THEN 'November' WHEN strftime('%m', utility_expense_period) = '12' THEN 'December' END AS Month,\n"
                + "       utility_expense_total AS total\n"
                + "  FROM utility_expense\n"
                + " WHERE utility_expense_type = 4 \n"
                + " GROUP BY Month\n"
                + " ORDER BY strftime('%m', utility_expense_period)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportElectricityU obj = ReportElectricityU.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportInternetU> getInternetByMounth() {
        ArrayList<ReportInternetU> list = new ArrayList();
        String sql = "SELECT CASE WHEN strftime('%m', utility_expense_period) = '01' THEN 'January' WHEN strftime('%m', utility_expense_period) = '02' THEN 'February' WHEN strftime('%m', utility_expense_period) = '03' THEN 'March' WHEN strftime('%m', utility_expense_period) = '04' THEN 'April' WHEN strftime('%m', utility_expense_period) = '05' THEN 'May' WHEN strftime('%m', utility_expense_period) = '06' THEN 'June' WHEN strftime('%m', utility_expense_period) = '07' THEN 'July' WHEN strftime('%m', utility_expense_period) = '08' THEN 'August' WHEN strftime('%m', utility_expense_period) = '09' THEN 'September' WHEN strftime('%m', utility_expense_period) = '10' THEN 'October' WHEN strftime('%m', utility_expense_period) = '11' THEN 'November' WHEN strftime('%m', utility_expense_period) = '12' THEN 'December' END AS Month,\n"
                + "       utility_expense_total AS total\n"
                + "  FROM utility_expense\n"
                + " WHERE utility_expense_type = 1 \n"
                + " GROUP BY Month\n"
                + " ORDER BY strftime('%m', utility_expense_period)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportInternetU obj = ReportInternetU.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportRentalU> getRentalByMounth() {
        ArrayList<ReportRentalU> list = new ArrayList();
        String sql = "SELECT CASE WHEN strftime('%m', utility_expense_period) = '01' THEN 'January' WHEN strftime('%m', utility_expense_period) = '02' THEN 'February' WHEN strftime('%m', utility_expense_period) = '03' THEN 'March' WHEN strftime('%m', utility_expense_period) = '04' THEN 'April' WHEN strftime('%m', utility_expense_period) = '05' THEN 'May' WHEN strftime('%m', utility_expense_period) = '06' THEN 'June' WHEN strftime('%m', utility_expense_period) = '07' THEN 'July' WHEN strftime('%m', utility_expense_period) = '08' THEN 'August' WHEN strftime('%m', utility_expense_period) = '09' THEN 'September' WHEN strftime('%m', utility_expense_period) = '10' THEN 'October' WHEN strftime('%m', utility_expense_period) = '11' THEN 'November' WHEN strftime('%m', utility_expense_period) = '12' THEN 'December' END AS Month,\n"
                + "       utility_expense_total AS total\n"
                + "  FROM utility_expense\n"
                + " WHERE utility_expense_type = 2 \n"
                + " GROUP BY Month\n"
                + " ORDER BY strftime('%m', utility_expense_period)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportRentalU obj = ReportRentalU.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportWaterU> getWaterByMounth() {
        ArrayList<ReportWaterU> list = new ArrayList();
        String sql = "SELECT CASE WHEN strftime('%m', utility_expense_period) = '01' THEN 'January' WHEN strftime('%m', utility_expense_period) = '02' THEN 'February' WHEN strftime('%m', utility_expense_period) = '03' THEN 'March' WHEN strftime('%m', utility_expense_period) = '04' THEN 'April' WHEN strftime('%m', utility_expense_period) = '05' THEN 'May' WHEN strftime('%m', utility_expense_period) = '06' THEN 'June' WHEN strftime('%m', utility_expense_period) = '07' THEN 'July' WHEN strftime('%m', utility_expense_period) = '08' THEN 'August' WHEN strftime('%m', utility_expense_period) = '09' THEN 'September' WHEN strftime('%m', utility_expense_period) = '10' THEN 'October' WHEN strftime('%m', utility_expense_period) = '11' THEN 'November' WHEN strftime('%m', utility_expense_period) = '12' THEN 'December' END AS Month,\n"
                + "       utility_expense_total AS total\n"
                + "  FROM utility_expense\n"
                + " WHERE utility_expense_type = 3 \n"
                + " GROUP BY Month\n"
                + " ORDER BY strftime('%m', utility_expense_period)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportWaterU obj = ReportWaterU.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

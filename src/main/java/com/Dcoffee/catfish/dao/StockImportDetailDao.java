/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.model.StockImportDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class StockImportDetailDao implements Dao<StockImportDetail> {

//    @Override
//    public StockImportDetail get(int id) {
//        StockImportDetail stock_import_detail = null;
//        String sql = "SELECT * FROM stock_import_detail WHERE id=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, id);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                stock_import_detail = StockImportDetail.fromRS(rs);
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return stock_import_detail;
//    }
    @Override
    public List<StockImportDetail> getAll() {
        ArrayList<StockImportDetail> list = new ArrayList();
        String sql = "SELECT * FROM stock_import_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImportDetail stock_import_detail = StockImportDetail.fromRS(rs);
                list.add(stock_import_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockImportDetail save(StockImportDetail obj) {
        String sqlInsert = "INSERT INTO stock_import_detail (stock_import_id, material_id, material_name, material_unit, material_qty, material_price, material_totalPrice,stock_import_detail_company) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        //String sqlUpdateMaterial = "UPDATE material SET material_qty = ? WHERE material_id = ?";

        Connection conn = DatabaseHelper.getConnect();

        try {
            // เพิ่มข้อมูลในตาราง check_store_detail
            PreparedStatement stmtInsert = conn.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS);
            stmtInsert.setInt(1, obj.getStockImportId());
            stmtInsert.setInt(2, obj.getMaterialId());
            stmtInsert.setString(3, obj.getMaterialName());
            stmtInsert.setString(4, obj.getMaterialUnit());
            stmtInsert.setInt(5, obj.getMaterialQty());
            stmtInsert.setInt(6, obj.getMaterialPrice());
            stmtInsert.setInt(7, obj.getMaterialTotalPrice());
            stmtInsert.setString(8, obj.getStockImportDetailCompany());
            

            stmtInsert.executeUpdate();

            // รับคีย์หลักหลังการเพิ่ม
//            ResultSet generatedKeys = stmtInsert.getGeneratedKeys();
//            if (generatedKeys.next()) {
//                int id = generatedKeys.getInt(1);
//                obj.setId(id);
//            }

            // อัปเดตข้อมูลในตาราง material
            // Update
            String updateRemainSql = "UPDATE material SET material_totalPrice = material_totalPrice + ?, material_qty = material_qty + ? WHERE material_id = ?";
            PreparedStatement stmtUpdateMaterial = conn.prepareStatement(updateRemainSql);
            //PreparedStatement stmtUpdateMaterial = conn.prepareStatement(sqlUpdateMaterial);
            stmtUpdateMaterial.setInt(1, obj.getMaterialTotalPrice()); // อัปเดตจำนวนวัสดุ
            stmtUpdateMaterial.setInt(2, obj.getMaterialQty());
            stmtUpdateMaterial.setInt(3, obj.getMaterialId());

            stmtUpdateMaterial.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

        return obj;
    }

    @Override
    public StockImportDetail update(StockImportDetail obj) {
//        String sql = "UPDATE stock_import_detail"
//                + " SET stock_import_id = ?, material_id = ?, material_name = ?, material_unit = ?, material_qty = ?, material_price = ?, material_totalprice = ?, stock_import_detail_company = ?"
//                + " WHERE id = ?";
        String sql = "UPDATE stock_import_detail"
                + " SET material_qty = ?, stock_import_detail_company = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getStockImportId());
            stmt.setInt(2, obj.getMaterialId());
            stmt.setString(3, obj.getMaterialName());
            stmt.setString(4, obj.getMaterialUnit());
            stmt.setInt(5, obj.getMaterialQty());
            stmt.setInt(6, obj.getMaterialPrice());
            stmt.setInt(7, obj.getMaterialTotalPrice());
            stmt.setString(8, obj.getStockImportDetailCompany());
            stmt.setInt(9, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockImportDetail obj) {
        String sql = "DELETE FROM stock_import_detail WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<StockImportDetail> getAll(String where, String order) {
        ArrayList<StockImportDetail> list = new ArrayList();
        String sql = "SELECT * FROM stock_import_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImportDetail stock_import_detail = StockImportDetail.fromRS(rs);
                list.add(stock_import_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<StockImportDetail> getAll(String order) {
        ArrayList<StockImportDetail> list = new ArrayList();
        String sql = "SELECT * FROM stock_import_detail ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImportDetail stock_import_detail = StockImportDetail.fromRS(rs);
                list.add(stock_import_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public StockImportDetail get(int id) {
        StockImportDetail hisInvoiceDetail = null;
        String sql = "SELECT * FROM stock_import_detail WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                hisInvoiceDetail = StockImportDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hisInvoiceDetail;
    }

    public List<StockImportDetail> getAllById(int id) {
        ArrayList<StockImportDetail> list = new ArrayList();
        String sql = "SELECT * FROM stock_import_detail WHERE stock_import_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                StockImportDetail stock_import_detail = StockImportDetail.fromRS(rs);
                list.add(stock_import_detail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.Order;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fewxi
 */
public class CheckStoreDao implements Dao<CheckStore> {

    @Override
    public CheckStore get(int id) {
        CheckStore check_store = null;
        String sql = "SELECT * FROM check_store WHERE check_store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                check_store = CheckStore.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return check_store;
    }

    public List<CheckStore> getAll() {
        ArrayList<CheckStore> list = new ArrayList();
        String sql = "SELECT * FROM check_store";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStore check_store = CheckStore.fromRS(rs);
                list.add(check_store);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckStore> getAll(String where, String order) {
        ArrayList<CheckStore> list = new ArrayList();
        String sql = "SELECT * FROM check_store where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStore check_store = CheckStore.fromRS(rs);
                list.add(check_store);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStore> getDate(String dateTime) {
        ArrayList<CheckStore> list = new ArrayList<>();
        String sql = "SELECT check_store_id, DATE(check_store_date) AS check_store_date, user_id\n"
                + "FROM check_store\n"
                + "WHERE DATE(check_store_date) = ?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateTime);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CheckStore item = CheckStore.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStore> getAll(String order) {
        ArrayList<CheckStore> list = new ArrayList();
        String sql = "SELECT * FROM check_store ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStore check_store = CheckStore.fromRS(rs);
                list.add(check_store);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStore save(CheckStore obj) {

        String sql = "INSERT INTO check_store (user_id)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.getId());
//            stmt.setString(2, obj.getCheckstoreDate());
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//            stmt.setString(1, df.format(obj.getCheckstoreDate()));
//            Date utilDate = obj.getCheckstoreDate();
//            Timestamp timestamp = new Timestamp(utilDate.getTime());
//            stmt.setTimestamp(1, timestamp);
//            System.out.println(stmt);
            stmt.setInt(1, obj.getUserId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStore update(CheckStore obj) {
        String sql = "UPDATE check_store"
                + " SET check_store_date = ? user_id = ?"
                + " WHERE check_store_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(1, df.format(obj.getCheckstoreDate()));
//            stmt.setString(1, obj.getCheckstoreDate());
            stmt.setInt(2, obj.getUserId());
            stmt.setInt(3, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStore obj) {
        String sql = "DELETE FROM check_store WHERE check_store_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}

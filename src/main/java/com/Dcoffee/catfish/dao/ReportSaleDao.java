/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.ReportBackMounth;
import com.Dcoffee.catfish.model.ReportEmployee;
import com.Dcoffee.catfish.model.ReportImportLast;
import com.Dcoffee.catfish.model.ReportPayroll;
import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportProductBack;
import com.Dcoffee.catfish.model.ReportQty;
import com.Dcoffee.catfish.model.ReportSalary;
import com.Dcoffee.catfish.model.ReportSale;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportThisMouth;
import com.Dcoffee.catfish.model.ReportTime;
import com.Dcoffee.catfish.model.ReportToday;
import com.Dcoffee.catfish.model.ReportTotal;
import com.Dcoffee.catfish.model.ReportUtilitys;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ReportSaleDao implements Dao<ReportTotal> {

    @Override
    public ReportTotal get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportTotal> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportTotal save(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportTotal update(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportTotal> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ReportSaleM> getReportSaleByMounth() {
        {
            ArrayList<ReportSaleM> list = new ArrayList();
            String sql = "SELECT \n"
                    + "  CASE \n"
                    + "    WHEN strftime('%m', order_datetime) = '01' THEN 'January'\n"
                    + "    WHEN strftime('%m', order_datetime) = '02' THEN 'February'\n"
                    + "    WHEN strftime('%m', order_datetime) = '03' THEN 'March'\n"
                    + "    WHEN strftime('%m', order_datetime) = '04' THEN 'April'\n"
                    + "    WHEN strftime('%m', order_datetime) = '05' THEN 'May'\n"
                    + "    WHEN strftime('%m', order_datetime) = '06' THEN 'June'\n"
                    + "    WHEN strftime('%m', order_datetime) = '07' THEN 'July'\n"
                    + "    WHEN strftime('%m', order_datetime) = '08' THEN 'August'\n"
                    + "    WHEN strftime('%m', order_datetime) = '09' THEN 'September'\n"
                    + "    WHEN strftime('%m', order_datetime) = '10' THEN 'October'\n"
                    + "    WHEN strftime('%m', order_datetime) = '11' THEN 'November'\n"
                    + "    WHEN strftime('%m', order_datetime) = '12' THEN 'December'\n"
                    + "  END AS Month,\n"
                    + "  SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime\n"
                    + "GROUP BY Month\n"
                    + "ORDER BY strftime('%m', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleM obj = ReportSaleM.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleM> getReportSaleByMounth(String begin, String end) {
        {
            ArrayList<ReportSaleM> list = new ArrayList();
            String sql = "SELECT \n"
                    + "  CASE \n"
                    + "    WHEN strftime('%m', order_datetime) = '01' THEN 'January'\n"
                    + "    WHEN strftime('%m', order_datetime) = '02' THEN 'February'\n"
                    + "    WHEN strftime('%m', order_datetime) = '03' THEN 'March'\n"
                    + "    WHEN strftime('%m', order_datetime) = '04' THEN 'April'\n"
                    + "    WHEN strftime('%m', order_datetime) = '05' THEN 'May'\n"
                    + "    WHEN strftime('%m', order_datetime) = '06' THEN 'June'\n"
                    + "    WHEN strftime('%m', order_datetime) = '07' THEN 'July'\n"
                    + "    WHEN strftime('%m', order_datetime) = '08' THEN 'August'\n"
                    + "    WHEN strftime('%m', order_datetime) = '09' THEN 'September'\n"
                    + "    WHEN strftime('%m', order_datetime) = '10' THEN 'October'\n"
                    + "    WHEN strftime('%m', order_datetime) = '11' THEN 'November'\n"
                    + "    WHEN strftime('%m', order_datetime) = '12' THEN 'December'\n"
                    + "  END AS Month,\n"
                    + "  SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime >= ? AND order_datetime <= ?\n"
                    + "GROUP BY Month\n"
                    + "ORDER BY strftime('%m', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, begin);
                stmt.setString(2, end);
                ResultSet rs = stmt.executeQuery(); // ใช้ executeQuery() แทน stmt.executeQuery(sql);

                while (rs.next()) {
                    String month = rs.getString("Month");
                    double totalSum = rs.getDouble("total_sum");
                    ReportSaleM obj = new ReportSaleM(month, totalSum);
                    list.add(obj);
                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleD> getReportSaleByDay() {
        {
            ArrayList<ReportSaleD> list = new ArrayList();
            String sql = "SELECT DATE(order_datetime) as Day, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime >= '2023-05-15' AND order_datetime <= '2023-10-17'\n"
                    + "GROUP BY DATE(order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleD obj = ReportSaleD.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportImportLast> getReportImportLast() {
        {
            ArrayList<ReportImportLast> list = new ArrayList();
            String sql = "SELECT SUM(stock_import_total) AS total_import_for_current_month\n"
                    + "FROM stock_import\n"
                    + "WHERE strftime('%Y-%m', stock_import_date) = strftime('%Y-%m', 'now');";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportImportLast obj = ReportImportLast.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleD> getReportSaleByDay(String begin, String end) {
        {
            ArrayList<ReportSaleD> list = new ArrayList();
            String sql = "SELECT DATE(order_datetime) as Day, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime >= ? AND order_datetime <= ?\n"
                    + "GROUP BY DATE(order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, begin);
                stmt.setString(2, end);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    String day = rs.getString("Day");
                    double totalSum = rs.getDouble("total_sum");
                    ReportSaleD obj = new ReportSaleD(day, totalSum);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleW> getReportSaleByWeek() {
        {
            ArrayList<ReportSaleW> list = new ArrayList();
            String sql = "SELECT DATE(order_datetime, 'weekday 0', '-7 days') as WeekStart, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime\n"
                    + "GROUP BY DATE(order_datetime, 'weekday 0', '-7 days');";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleW obj = ReportSaleW.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleW> getReportSaleByWeek(String begin, String end) {
        {
            ArrayList<ReportSaleW> list = new ArrayList();
            String sql = "SELECT DATE(order_datetime, 'weekday 0', '-7 days') as WeekStart, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime >= DATE(?, 'weekday 0', '-7 days')\n"
                    + "      AND order_datetime <= DATE(?, 'weekday 0', '-7 days')\n"
                    + "GROUP BY DATE(order_datetime, 'weekday 0', '-7 days');";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, begin);
                stmt.setString(2, end);
                ResultSet rs = stmt.executeQuery(); // ใช้ executeQuery() แทน stmt.executeQuery(sql);

                while (rs.next()) {
                    String week = rs.getString("WeekStart");
                    double totalSum = rs.getDouble("total_sum");
                    ReportSaleW obj = new ReportSaleW(week, totalSum);
                    list.add(obj);
                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleH> getReportSaleByHour() {
        {
            ArrayList<ReportSaleH> list = new ArrayList();
            String sql = "SELECT strftime('%Y-%m-%d %H:00:00', order_datetime) as HourStart, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "GROUP BY strftime('%Y-%m-%d %H:00:00', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleH obj = ReportSaleH.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleH> getReportSaleByHour(String begin) {
        {
            ArrayList<ReportSaleH> list = new ArrayList();
            String sql = "SELECT strftime('%Y-%m-%d %H:00:00', order_datetime) as HourStart, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE DATE(order_datetime) = ?\n"
                    + "GROUP BY strftime('%Y-%m-%d %H:00:00', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, begin);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    String hour = rs.getString("HourStart");
                    double totalSum = rs.getDouble("total_sum");
                    ReportSaleH obj = new ReportSaleH(hour, totalSum);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSale> getYearReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt GROUP BY strftime(\"%Y\", receipt_timestamp)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getSelectedYearReport(String selectedYear) {

        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt WHERE strftime(\"%Y\", receipt_timestamp) = " + "\"" + selectedYear + "\" " + "GROUP BY strftime(\"%m\", receipt_timestamp) ORDER BY total ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getPayrollMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", check_time_in) as period"
                + "FROM check_time\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getSelectedNameReport(String selectedName) {

        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT employee_name FROM employee WHERE * = " + "\"" + selectedName + "\" ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportTime> getCheckEmp() {
        ArrayList<ReportTime> list = new ArrayList();
        String sql = "SELECT \n"
                + "    e.employee_id,\n"
                + "    u.user_name,\n"
                + "    CASE \n"
                + "        WHEN MIN(ct.check_time_in) IS NOT NULL THEN 'Arrived'\n"
                + "        ELSE 'Out of office'\n"
                + "    END AS attendance_status,\n"
                + "    COALESCE(MIN(ct.check_time_in), '-') AS check_in_time,\n"
                + "    COALESCE(MAX(ct.check_time_out), '-') AS check_out_time,\n"
                + "    CASE\n"
                + "        WHEN MIN(ct.check_time_in) IS NOT NULL THEN TIME(SUM(STRFTIME('%s', ct.check_time_out) - STRFTIME('%s', ct.check_time_in)), 'unixepoch')\n"
                + "        ELSE '-'\n"
                + "    END AS total_work_hours\n"
                + "FROM user u\n"
                + "INNER JOIN employee e ON u.employee_id = e.employee_id\n"
                + "LEFT JOIN (\n"
                + "    SELECT employee_id, check_time_in, check_time_out\n"
                + "    FROM check_time\n"
                + "    WHERE DATE(check_time_in) = DATE('now')\n"
                + ") ct ON ct.employee_id = e.employee_id\n"
                + "WHERE u.user_role != 0\n"
                + "GROUP BY e.employee_id, u.user_name;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportTime item = ReportTime.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportToday> getTodayReport() {

        ArrayList<ReportToday> list = new ArrayList();
        String sql = "SELECT\n"
                + "    SUM(total) as TotalSales,\n"
                + "    COUNT(order_id) as TotalOrders\n"
                + "FROM\n"
                + "    orders\n"
                + "WHERE\n"
                + "    DATE(order_datetime) = DATE('now');";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportToday item = ReportToday.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportToday> getYesterdayReport() {

        ArrayList<ReportToday> list = new ArrayList();
        String sql = "SELECT SUM(total) as TotalSales, COUNT(order_id) as TotalOrders\n"
                + "FROM orders\n"
                + "WHERE DATE(order_datetime) = DATE('now', '-1 day');";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportToday item = ReportToday.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportQty> getTodayReportQty() {

        ArrayList<ReportQty> list = new ArrayList();
        String sql = "SELECT o.order_datetime, op.product_name, op.product_size, op.product_type, op.product_level, op.amount\n"
                + "FROM orders o\n"
                + "INNER JOIN order_product op ON o.order_id = op.order_id\n"
                + "WHERE DATE(o.order_datetime) = DATE('now');";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportQty item = ReportQty.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportQty> getYesterdayReportQty() {

        ArrayList<ReportQty> list = new ArrayList();
        String sql = "SELECT o.order_datetime, op.product_name, op.product_size, op.product_type, op.product_level, op.amount\n"
                + "FROM orders o\n"
                + "INNER JOIN order_product op ON o.order_id = op.order_id\n"
                + "WHERE DATE(o.order_datetime) = DATE('now', '-1 day');";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportQty item = ReportQty.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportBackMounth> getReportBackMounth() {

        ArrayList<ReportBackMounth> list = new ArrayList();
        String sql = "SELECT strftime('%Y-%m', order_datetime) AS month,\n"
                + "       SUM(total) AS total_sum,\n"
                + "       SUM(discount) AS discount_sum\n"
                + "FROM orders\n"
                + "WHERE strftime('%Y-%m', order_datetime) = strftime('%Y-%m', 'now')\n"
                + "GROUP BY month\n"
                + "HAVING month = strftime('%Y-%m', 'now');";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportBackMounth item = ReportBackMounth.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportPayroll> getReportPayroll() {

        ArrayList<ReportPayroll> list = new ArrayList();
        String sql = "SELECT strftime('%Y-%m', check_time_in) AS month,\n"
                + "       SUM(check_time_hour * 45) AS total_payment\n"
                + "FROM check_time\n"
                + "WHERE strftime('%Y-%m', check_time_in) = strftime('%Y-%m', 'now')\n"
                + "GROUP BY month;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportPayroll item = ReportPayroll.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportUtilitys> getReportUtilitys() {

        ArrayList<ReportUtilitys> list = new ArrayList();
        String sql = "SELECT strftime('%Y-%m', utility_expense_period) AS current_month, SUM(utility_expense_total)\n"
                + "FROM utility_expense\n"
                + "WHERE strftime('%Y-%m', utility_expense_period) = strftime('%Y-%m', 'now')\n"
                + "GROUP BY current_month;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportUtilitys item = ReportUtilitys.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportProductBack> getReportProductBack() {

        ArrayList<ReportProductBack> list = new ArrayList();
        String sql = "SELECT op.product_name, SUM(op.amount) AS total_sold, SUM(op.total) AS total_price\n"
                + "FROM order_product op\n"
                + "WHERE op.order_id IN (\n"
                + "  SELECT o.order_id\n"
                + "  FROM orders o\n"
                + "  WHERE strftime('%Y-%m', o.order_datetime) = strftime('%Y-%m', 'now')\n"
                + ")\n"
                + "GROUP BY op.product_name\n"
                + "ORDER BY total_price DESC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportProductBack item = ReportProductBack.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSalary> getReportSalary() {

        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT \n"
                + "       e.employee_name,\n"
                + "       e.employee_surename,\n"
                + "       e.employee_hourly_rate,\n"
                + "       c.check_time_in,\n"
                + "       c.check_time_out,\n"
                + "       c.check_time_hour,\n"
                + "       p.payroll_date,\n"
                + "       c.check_time_hour * e.employee_hourly_rate AS total\n"
                + "  FROM check_time c\n"
                + "       INNER JOIN payroll p ON c.payroll_id = p.payroll_id\n"
                + "       INNER JOIN employee e ON c.employee_id = e.employee_id\n"
                + "WHERE c.check_time_payment = 'y';";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportEmployee> getReportEmployee(int EmpId) {
        {
            ArrayList<ReportEmployee> list = new ArrayList();
            String sql = "SELECT \n"
                    + "       c.check_time_in,\n"
                    + "       c.check_time_out,\n"
                    + "       c.check_time_hour,\n"
                    + "       CASE WHEN c.check_time_payment = 'n' THEN 'No Payment' ELSE \n"
                    + "           (CASE WHEN c.check_time_payment = 'y' THEN p.payroll_date ELSE NULL END)\n"
                    + "       END AS payroll_date,\n"
                    + "       (e.employee_hourly_rate * c.check_time_hour) AS Income\n"
                    + "  FROM check_time c\n"
                    + "  LEFT JOIN payroll p ON c.payroll_id = p.payroll_id\n"
                    + "  LEFT JOIN employee e ON c.employee_id = e.employee_id\n"
                    + "  WHERE (c.check_time_payment = 'n' OR (c.check_time_payment = 'y' AND p.payroll_id IS NOT NULL))\n"
                    + "    AND DATE(c.check_time_in) BETWEEN DATE('now', '-20 days') AND DATE('now')\n"
                    + "    AND c.employee_id = ?;";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, EmpId);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    String timein = rs.getString("check_time_in");
                    String timeout = rs.getString("check_time_out");
                    int timehour = rs.getInt("check_time_hour");
                    String date = rs.getString("payroll_date");
                    double income = rs.getDouble("income");
                    ReportEmployee obj = new ReportEmployee(timein, timeout, timehour, date, income);
                    list.add(obj);
                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportThisMouth> getReportThisMouth(int EmpId) {
        {
            ArrayList<ReportThisMouth> list = new ArrayList();
            String sql = "SELECT SUM(check_time_hour) AS TotalCheckTime\n"
                    + "FROM check_time\n"
                    + "WHERE employee_id = ?\n"
                    + "  AND strftime('%Y-%m', check_time_in) = strftime('%Y-%m', 'now');";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, EmpId);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    int totalCheckTime = rs.getInt("TotalCheckTime");
                    ReportThisMouth obj = new ReportThisMouth(totalCheckTime);
                    list.add(obj);
                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSalary> searchReportSalaryByName(String search) {

        ArrayList<ReportSalary> list = new ArrayList();
        String sql = "SELECT\n"
                + "e.employee_name,\n"
                + "       e.employee_surename,\n"
                + "       e.employee_hourly_rate,\n"
                + "       c.check_time_in,\n"
                + "       c.check_time_out,\n"
                + "       c.check_time_hour,\n"
                + "       p.payroll_date,\n"
                + "       c.check_time_hour * e.employee_hourly_rate AS total\n"
                + "  FROM check_time c\n"
                + "       INNER JOIN payroll p ON c.payroll_id = p.payroll_id\n"
                + "       INNER JOIN employee e ON c.employee_id = e.employee_id\n"
                + "WHERE c.check_time_payment = 'y'\n"
                + "       AND e.employee_name LIKE ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "%"+search+"%");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ReportSalary item = ReportSalary.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

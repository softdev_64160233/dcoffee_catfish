/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.StockImport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class StockImportDao implements Dao<StockImport> {

    @Override
    public StockImport get(int id) {
        StockImport stock_import = null;
        String sql = "SELECT * FROM stock_import WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock_import = StockImport.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stock_import;
    }

    @Override
    public List<StockImport> getAll() {
        ArrayList<StockImport> list = new ArrayList();
        String sql = "SELECT * FROM stock_import";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImport stock_import = StockImport.fromRS(rs);
                list.add(stock_import);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<StockImport> getAll(String order) {
        ArrayList<StockImport> list = new ArrayList();
        String sql = "SELECT * FROM stock_import  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                StockImport stockImport = StockImport.fromRS(rs);
                list.add(stockImport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<StockImport> getDate(String dateTime) {
    ArrayList<StockImport> list = new ArrayList<>();
    String sql = "SELECT id, strftime('%Y-%m-%d', stock_import_date) AS stock_import_date, user_id, stock_import_total\n"
            + "FROM stock_import\n"
            + "WHERE strftime('%Y-%m-%d', stock_import_date) = ?;";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, dateTime);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            StockImport item = StockImport.fromRS(rs);
            list.add(item);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}


    @Override
    public StockImport save(StockImport obj) {
        String sql = "INSERT INTO stock_import (stock_import_total, user_id)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            //stmt.setDate(1, obj.getCreatedDate());
            stmt.setInt(1, obj.getStockImportTotal());
            stmt.setInt(2, obj.getUserId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public StockImport update(StockImport obj) {
        String sql = "UPDATE stock_import"
                + " SET user_id = ?, stock_import_total = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getStockImportTotal());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(StockImport obj) {
        String sql = "DELETE FROM stock_import WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<StockImport> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}

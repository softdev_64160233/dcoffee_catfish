/*
 * Click nbfs://nbhost/SystemFileSystem/Tuserlates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Tuserlates/Classes/Class.java to edit this tuserlate
 */
package com.Dcoffee.catfish.dao;


import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class OrderDao implements Dao<Order> {

    @Override
    public Order get(int id) {
        Order item = null;
        String sql = "SELECT * FROM \'orders\' WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Order.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public Order getLast() {
        Order item = null;
        String sql = "SELECT * FROM \'orders\' ORDER BY order_id DESC LIMIT 1";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Order.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<Order> getAll() {
        ArrayList<Order> list = new ArrayList();
        String sql = "SELECT * FROM \'orders\'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Order item = Order.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Order> getAll(String where, String order) {
        ArrayList<Order> list = new ArrayList();
        String sql = "SELECT * FROM \'orders\' where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Order item = Order.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Order> getAll(String order) {
        ArrayList<Order> list = new ArrayList();
        String sql = "SELECT * FROM \'orders\'  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Order item = Order.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Order> getAll(int id) {
        ArrayList<Order> list = new ArrayList();
        String sql = "SELECT * FROM \'orders\' WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Order item = Order.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Order save(Order obj) {

        String sql = "INSERT INTO \'orders\' (order_id,cus_id, user_id, total, discount, cash, change, order_datetime)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrderId());
            stmt.setInt(2, obj.getCusId());
            stmt.setInt(3, obj.getUserId());
            stmt.setDouble(4, obj.getOrderTotal());
            stmt.setDouble(5, obj.getOrderDiscount());
            stmt.setDouble(6, obj.getOrderCash());
            stmt.setDouble(7, obj.getOrderChange());
            stmt.setString(8, obj.getOrderDateTime());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setCusId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Order update(Order obj) {
        return null;
    }

    @Override
    public int delete(Order obj) {
        String sql = "DELETE FROM \'orders\' WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getOrderId());
            int ret = stmt.executeUpdate();
            System.out.println("pass execute");
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
     public List<Order> getDate(String dateTime) {
    ArrayList<Order> list = new ArrayList<>();
    String sql = "SELECT * FROM orders WHERE DATE(order_datetime) = ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, dateTime);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            Order item = Order.fromRS(rs);
            list.add(item);
        }
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
    }
    return list;
}

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.dao.CheckStoreDao;
import com.Dcoffee.catfish.dao.CheckTimeDao;
import com.Dcoffee.catfish.dao.ReportSaleDao;
import com.Dcoffee.catfish.model.CheckStore;
import com.Dcoffee.catfish.model.CheckTime;
import com.Dcoffee.catfish.model.Customer;
import com.Dcoffee.catfish.model.ReportEmployee;
import com.Dcoffee.catfish.model.ReportImportLast;
import com.Dcoffee.catfish.model.ReportQty;
import com.Dcoffee.catfish.model.ReportSalary;
import com.Dcoffee.catfish.model.ReportToday;
import com.Dcoffee.catfish.model.ReportUtilitys;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Miso
 */
public class TestCheck {

    public static void main(String[] args) {
        // getall
        CheckTimeDao checkstoreDao = new CheckTimeDao();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = "2023-11-04"; // วันที่ที่คุณต้องการตรวจสอบ

        try {
            Date date = dateFormat.parse(dateString);
            boolean hasRecord = checkstoreDao.hasRecordForDate(date);
            System.out.println(hasRecord);
            
            if (hasRecord) {
                // มีรายการที่ตรงกับเงื่อนไข
                System.out.println("T");
            } else {
                // ไม่มีรายการที่ตรงกับเงื่อนไข
                System.out.println("F");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ReportSaleDao dao = new ReportSaleDao();
//        List<ReportQty> test = dao.getTodayReportQty();
// List<ReportUtility> test = dao.getReportUtilitys();
//List<ReportEmployee> test = dao.getReportEmployee(5);
//List<ReportImportLast> test = dao.getReportImportLast();

        List<ReportUtilitys> test = dao.getReportUtilitys();
//List<ReportToday> test = dao.getTodayReport();
//List<ReportSalary> test = dao.getReportSalary();
        System.out.print(test
        );
        
    }
}

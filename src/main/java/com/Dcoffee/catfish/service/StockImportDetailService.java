/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.StockImportDetailDao;
import com.Dcoffee.catfish.model.StockImportDetail;
import java.util.List;

/**
 *
 * @author User
 */
public class StockImportDetailService {
    public StockImportDetail getById(int id) {
        StockImportDetailDao promoDao = new StockImportDetailDao();
        return promoDao.get(id);
    }

    public List<StockImportDetail> getHistoryInvoiceDetails() {
        StockImportDetailDao promoDao = new StockImportDetailDao();
        return promoDao.getAll(" id asc");
    }
    
    public List<StockImportDetail> getHistoryInvoiceDetailsById(int id) {
        StockImportDetailDao promoDao = new StockImportDetailDao();
        return promoDao.getAllById(id);
    }
    public StockImportDetail add(StockImportDetail editedStockImportDetail) {
        StockImportDetailDao promoDao = new StockImportDetailDao();
        return promoDao.save(editedStockImportDetail);
    }

    public StockImportDetail update(StockImportDetail editedStockImportDetail) {
        StockImportDetailDao promoDao = new StockImportDetailDao();
        return promoDao.update(editedStockImportDetail);
    }

    public int delete(StockImportDetail editedStockImportDetail) {
        StockImportDetailDao promoDao = new StockImportDetailDao();
        return promoDao.delete(editedStockImportDetail);
    }

    public List<StockImportDetail> getDate(String datetime) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

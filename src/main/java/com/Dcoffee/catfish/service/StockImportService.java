/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.StockImportDao;
import com.Dcoffee.catfish.dao.StockImportDetailDao;
import com.Dcoffee.catfish.model.StockImport;
import com.Dcoffee.catfish.model.StockImportDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class StockImportService {
//    public StockImport add(StockImport editedStockImport) {
//        StockImportDao promoDao = new StockImportDao();
//        return promoDao.save(editedStockImport);
//    }
    
    public StockImport add(StockImport editedStockImport) {
        StockImportDao stockImportDao = new StockImportDao();
        StockImportDetailDao stockImportDetailDao = new StockImportDetailDao();
        StockImport stockImport = stockImportDao.save(editedStockImport);
        for(StockImportDetail hi: editedStockImport.getStockImportDetail()) {
            hi.setStockImportId(stockImport.getId());
            System.out.println(hi);
            stockImportDetailDao.save(hi);
            
        }
        return stockImport;
    }

    public StockImport update(StockImport editedStockImport) {
        StockImportDao promoDao = new StockImportDao();
        return promoDao.update(editedStockImport);
    }

    public int delete(StockImport editedStockImport) {
        StockImportDao promoDao = new StockImportDao();
        return promoDao.delete(editedStockImport);
    }
    
    public List<StockImport> getStockImportDetails() {
        StockImportDao checkstoreDao = new StockImportDao();
        return checkstoreDao.getAll();
    }
    
    public ArrayList<StockImport> getStockImport(){
       StockImportDao stockImporteDao = new StockImportDao();
        return (ArrayList<StockImport>) stockImporteDao.getAll(" id desc");
    }
    
    public ArrayList<StockImportDetail> getHisInvoiceDetails(){
        StockImportDetailDao hisInvoiceDetail = new StockImportDetailDao();
        return (ArrayList<StockImportDetail>) hisInvoiceDetail.getAll(" id asc");
    }
    
    public StockImport getById(int id) {
        StockImportDao promoDao = new StockImportDao();
        return promoDao.get(id);
    }

    public List<StockImport> getDate(String datetime) {
        StockImportDao stockImporteDao = new StockImportDao();
        return (ArrayList<StockImport>) stockImporteDao.getDate(datetime);
    }

     

   
    
}

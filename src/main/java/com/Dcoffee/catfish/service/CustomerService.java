/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.CustomerDao;
import com.Dcoffee.catfish.model.Customer;
import java.util.List;

/**
 *
 * @author Miso
 */
public class CustomerService {

    public List<Customer> getCustomers() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll(" cus_id asc");
    }
        public List<Customer> getOrderByCustomers() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll(" cus_point desc");
    }

    public Customer add(Customer editedCus) throws ValidateException {
        if (!editedCus.isValid()) {
            throw new ValidateException("ข้อมูลลูกค้าไม่ถูกต้อง โปรดใส่ข้อมูลให้ครบถ้วน");
        }
        CustomerDao cusDao = new CustomerDao();
        return cusDao.save(editedCus);
    }

    public Customer update(Customer editedCus) throws ValidateException {
        if (!editedCus.isValid()) {
            throw new ValidateException("Customer is invaild!!!");
        }
        CustomerDao cusDao = new CustomerDao();
        return cusDao.update(editedCus);
    }

    public int delete(Customer editedCus) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.delete(editedCus);
    }
    
    public Customer getCustomersTel(String tel) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getCustomersTel(tel);
    }
    
    public Customer addNew(Customer editedCus) throws ValidateException {
         if (!editedCus.isValid()) {
            throw new ValidateException("Customer is invaild!!!");
        }
        CustomerDao cusDao = new CustomerDao();
        return cusDao.save(editedCus);
    }

    public Customer getLast() {
        CustomerDao cusrDao = new CustomerDao();
        return cusrDao.getLast();
    }
}

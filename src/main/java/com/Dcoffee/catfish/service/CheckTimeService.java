/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.CheckTimeDao;
import com.Dcoffee.catfish.dao.PayrollDao;
import com.Dcoffee.catfish.model.CheckTime;
import com.Dcoffee.catfish.model.Payroll;
import com.Dcoffee.catfish.model.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Miso
 */
public class CheckTimeService {

    public static List<CheckTime> getCheckTime() {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getAll();
    }
//        public static List<CheckTime> getOrderByCheckId() {
//        CheckTimeDao checkTimeDao = new CheckTimeDao();
//        return checkTimeDao.getAll(" check_time_id desc");
//    }

    public static String formatedDate(Date date) {
//        Date date = checkTime.getIn();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }

    public static void login(User user) throws ParseException {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        Date dateOut = df.parse(df.format(date));
        CheckTime checkTime = new CheckTime(date, dateOut, 0, "n", user.getEmployeeId(), 0);
        checkTimeDao.save(checkTime);
    }

    public static void logout(int time) throws ParseException {
//        CheckTimeDao checkTimeDao = new CheckTimeDao();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        Date date = new Date();
//        Date dateOut = df.parse(df.format(date));
//        CheckTime checkTime = new CheckTime(date, dateOut, 0, "n", user.getEmployeeId(), 0);
//        checkTimeDao.update(checkTime);

    }
    public List<Payroll> getID(int id) {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.getAll(id);
    }

    public static void changePayStatus(CheckTime checktime, int payrollId) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checktime.setPayment("y");
        checktime.setPayrollId(payrollId);
        checkTimeDao.update(checktime);
    }

     public List<CheckTime> getReportByYear(String year, int index) {
        CheckTimeDao payrollReportDao = new CheckTimeDao();
        return CheckTimeDao.getYearReport(year, index);
    }

    public List<CheckTime> getReportByMonthYear(String year, int index, String month) {
        CheckTimeDao payrollReportDao = new CheckTimeDao();
        return CheckTimeDao.getMonthYearReport(year, index, month);
    }
    public static List<CheckTime> SortPaymentY() {
        CheckTimeDao payrollReportDao = new CheckTimeDao();
        return CheckTimeDao.SortPaymentY();
    }
      public static List<CheckTime> SortPaymentN() {
        CheckTimeDao payrollReportDao = new CheckTimeDao();
        return CheckTimeDao.SortPaymentN();
    }
    
//    public CheckTime ChecktimeHour(int id) {
//    CheckTimeDao checkTimeDao = new CheckTimeDao();
//    return CheckTimeDao.updateHour(id);
//}
//    
//    public CheckTime updateHour(int id) {
//        CheckTimeDao checkDao = new CheckTimeDao();
//        
//        return checkDao.updateHour(id);
//    }
    
    public static int ChecktimeHour(int id) {
    CheckTimeDao checkTimeDao = new CheckTimeDao();
    return checkTimeDao.updateHour(id);
}
    
  

    
    



}

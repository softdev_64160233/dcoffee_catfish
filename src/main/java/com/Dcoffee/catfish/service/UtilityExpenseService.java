/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.OrderDao;
import com.Dcoffee.catfish.dao.UtilityExpenseDao;
import com.Dcoffee.catfish.model.Order;
import com.Dcoffee.catfish.model.UtilityExpense;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author HP
 */
public class UtilityExpenseService {
    public static String formatedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }
    
    public List<UtilityExpense> getUtilityExpenses() {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.getAll(" utility_expense_id asc");
    }

    public UtilityExpense add(UtilityExpense editedPromo) throws ValidateException {
         if (!editedPromo.isValid()) {
            throw new ValidateException("Utility Expense is invaild!!!");
        }
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        Date period = new Date();
        editedPromo.setUtilityExpensePeriod(period);
        return utilityDao.save(editedPromo);
    }

    public UtilityExpense update(UtilityExpense editedPromo) throws ValidateException {
        if (!editedPromo.isValid()) {
            throw new ValidateException("Utility Expense is invaild!!!");
        }
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.update(editedPromo);
    }

    public int delete(UtilityExpense editedPromo) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.delete(editedPromo);
    }

    public UtilityExpense getUtilityExpenseID(int id) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.get(id);
    }
    
    public List<UtilityExpense> getDate(String DateTime) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.getDate(DateTime);
    }
    public List<UtilityExpense> getDateByTime(String begin,String end) {
        UtilityExpenseDao utilityDao = new UtilityExpenseDao();
        return utilityDao.getDateByTime(begin, end);
    }
}

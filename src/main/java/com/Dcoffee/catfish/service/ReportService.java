/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.ReportProductDao;
import com.Dcoffee.catfish.dao.ReportSaleDao;
import com.Dcoffee.catfish.dao.ReportStockDao;
import com.Dcoffee.catfish.dao.ReportTotalCustomerDao;
import com.Dcoffee.catfish.dao.ReportUtilityDao;
import com.Dcoffee.catfish.model.ReportBackMounth;
import com.Dcoffee.catfish.model.ReportElectricityU;
import com.Dcoffee.catfish.model.ReportEmployee;
import com.Dcoffee.catfish.model.ReportImportLast;
import com.Dcoffee.catfish.model.ReportInternetU;
import com.Dcoffee.catfish.model.ReportPayroll;
import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportProductBack;
import com.Dcoffee.catfish.model.ReportQty;
import com.Dcoffee.catfish.model.ReportRentalU;
import com.Dcoffee.catfish.model.ReportSalary;
import com.Dcoffee.catfish.model.ReportSale;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportStock;
import com.Dcoffee.catfish.model.ReportThisMouth;
import com.Dcoffee.catfish.model.ReportTime;
import com.Dcoffee.catfish.model.ReportToday;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import com.Dcoffee.catfish.model.ReportUtilitys;
import com.Dcoffee.catfish.model.ReportUtilitysPage;
import com.Dcoffee.catfish.model.ReportWaterU;
import com.Dcoffee.catfish.ui.Report.ReportUtility;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ReportService {

    public List<ReportTotalCustomer> getTotalByCus() {
        ReportTotalCustomerDao reportDao = new ReportTotalCustomerDao();
        return reportDao.getTotalByCus();
    }

    public List<ReportTotalCustomer> getTotalByCusBack() {
        ReportTotalCustomerDao reportDao = new ReportTotalCustomerDao();
        return reportDao.getTotalByCusBack();
    }

    public List<ReportProduct> getBestProduct() {
        ReportProductDao reportDao = new ReportProductDao();
        return reportDao.getBestProduct();
    }

    public List<ReportStock> getOutStock() {
        ReportStockDao reportDao = new ReportStockDao();
        return reportDao.getOutStock();
    }

    public List<ReportSaleM> getReportSaleByMounth() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByMounth();
    }
    public List<ReportImportLast> getReportImportLast() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportImportLast();
    }

    public List<ReportSaleM> getReportSaleByMounth(String begin, String end) {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByMounth(begin, end);
    }

    public List<ReportSaleD> getReportSaleByDay() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByDay();
    }

    public List<ReportSaleD> getReportSaleByDay(String begin, String end) {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByDay(begin, end);
    }

    public List<ReportSaleW> getReportSaleByWeek() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByWeek();
    }

    public List<ReportSaleW> getReportSaleByWeek(String begin, String end) {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByWeek(begin, end);
    }

    public List<ReportSaleH> getReportSaleByHour() {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByHour();
    }

    public List<ReportSaleH> getReportSaleByHour(String begin) {
        ReportSaleDao reportDao = new ReportSaleDao();
        return reportDao.getReportSaleByHour(begin);
    }

    public List<ReportSale> getReportYear() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYearReport();
    }

    public List<ReportSale> getReportMonths() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }

    public List<ReportSale> getReportSaleBySelectedYear(String year) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getSelectedYearReport(year);
    }

    public List<ReportSale> getReportPayrollMonths() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }

    public List<ReportSale> getReporPayrollBySelectedName(String name) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getSelectedYearReport(name);
    }

    public List<ReportTime> getCheckEmp() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getCheckEmp();
    }

    public List<ReportToday> getReportToday() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getTodayReport();
    }

    public List<ReportToday> getYesterdayReport() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYesterdayReport();
    }

    public List<ReportQty> getTodayReportQty() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getTodayReportQty();
    }

    public List<ReportQty> getYesterdayReportQty() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getYesterdayReportQty();
    }

    public List<ReportBackMounth> getReportBackMounth() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportBackMounth();
    }

    public List<ReportPayroll> getReportPayroll() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportPayroll();
    }

    public List<ReportProductBack> getReportProductBack() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportProductBack();
    }

    public List<com.Dcoffee.catfish.model.ReportUtilitys> getReportUtilitys() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportUtilitys();
    }

    public List<ReportEmployee> getReportEmployee(int empId) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportEmployee(empId);
    }

    public List<ReportThisMouth> getReportThisMouth(int empId) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportThisMouth(empId);
    }

    public List<ReportUtilitysPage> getUtilityByMounth() {
        ReportUtilityDao dao = new ReportUtilityDao();
        return dao.getUtilityByMounth();
    }

    public List<ReportElectricityU> getElectricityByMounth() {
        ReportUtilityDao dao = new ReportUtilityDao();
        return dao.getElectricityByMounth();
    }

    public List<ReportRentalU> getRentalByMounth() {
        ReportUtilityDao dao = new ReportUtilityDao();
        return dao.getRentalByMounth();
    }

    public List<ReportWaterU> getWaterByMounth() {
        ReportUtilityDao dao = new ReportUtilityDao();
        return dao.getWaterByMounth();
    }

    public List<ReportInternetU> getInternetByMounth() {
        ReportUtilityDao dao = new ReportUtilityDao();
        return dao.getInternetByMounth();
    }

    public List<ReportSalary> getReportSalary() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getReportSalary();
    }
    public List<ReportSalary> searchReportSalaryByName(String search ) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.searchReportSalaryByName(search);
    }
}

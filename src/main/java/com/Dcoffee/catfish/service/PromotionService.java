/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.PromotionDao;
import com.Dcoffee.catfish.model.Promotion;
import java.util.List;

/**
 *
 * @author HP
 */
public class PromotionService {
    
    public List<Promotion> getPromotions() {
        PromotionDao promoDao = new PromotionDao();
        return promoDao.getAll(" promo_id asc");
    }

    public Promotion add(Promotion editedPromo) throws ValidateException {
         if (!editedPromo.isValid()) {
            throw new ValidateException("User is invaild!!!");
        }
        PromotionDao promoDao = new PromotionDao();
        return promoDao.save(editedPromo);
    }

    public Promotion update(Promotion editedPromo) throws ValidateException {
         if (!editedPromo.isValid()) {
            throw new ValidateException("User is invaild!!!");
        }
        PromotionDao promoDao = new PromotionDao();
        return promoDao.update(editedPromo);
    }

    public int delete(Promotion editedPromo) {
        PromotionDao promoDao = new PromotionDao();
        return promoDao.delete(editedPromo);
    }

    public Promotion getPromotionID(int id) {
        PromotionDao promoDao = new PromotionDao();
        return promoDao.get(id);
    }
}
